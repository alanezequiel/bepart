package utn.frgp.pa2.bepart.ui.moderador;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import utn.frgp.pa2.bepart.R;
import utn.frgp.pa2.bepart.Session.DocumentacionSession;
import utn.frgp.pa2.bepart.entity.Documentacion;
import utn.frgp.pa2.bepart.entity.EstadoDocumentacion;
import utn.frgp.pa2.bepart.model.DocumentacionModel;


public class SolicitudesOrganizador extends Fragment {

    ListView listView;
    ArrayList<Documentacion> docPendientes = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        if (container != null) {
            container.removeAllViews();
        }

        View view = inflater.inflate(R.layout.fragment_solicitudes_organizador, container, false);

        listView = view.findViewById(R.id.list_view);

        DocumentacionModel documentacionModel = new DocumentacionModel();
        Documentacion documentacion = new Documentacion();
        documentacion.setIdDocumentacion(-1);
        EstadoDocumentacion estadoDocumentacion = new EstadoDocumentacion();
        estadoDocumentacion.setIdEstadoDoc(EstadoDocumentacion.PENDIENTE_APROBACION); //Hardcodeo pendiente
        documentacion.setEstadoDoc(estadoDocumentacion);
        documentacion.setIdUsuario(-1); //Para que traiga todos los usuarios
        try {
            docPendientes = documentacionModel.select(documentacion);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        String[] pendientes = new String[docPendientes.size()];
        for(int i = 0; i < docPendientes.size(); i++){
            String comentario = docPendientes.get(i).getComentario();
            if( comentario.length() > 50){
                comentario = comentario.substring(0, 49);
            }
            pendientes[i] =
                    "Fecha : " + docPendientes.get(i).getFechaSolicitud() + "\n" +
                    "Nº Doc: " + docPendientes.get(i).getNroDoc() + "\n" +
                    "User  : " + docPendientes.get(i).getUsername();
        }
        ArrayAdapter<String> monthAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, pendientes);
        listView.setAdapter(monthAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                RevisionSolicitudOrganizador revSolOrg = new RevisionSolicitudOrganizador();
                Documentacion doc = docPendientes.get(position);
                DocumentacionSession docSession = new DocumentacionSession(getContext());
                docSession.loadDocumentacionToSessionData(doc);
                revSolOrg.show(getActivity().getSupportFragmentManager(), "RevisionSolicitudOrganizador");
            }
        });

        return view;
    }

}