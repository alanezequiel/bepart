package utn.frgp.pa2.bepart.recyclerview;

import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import utn.frgp.pa2.bepart.R;
import utn.frgp.pa2.bepart.entity.Evento;

public class RecyclerViewAdaptadorEvento extends RecyclerView.Adapter<RecyclerViewAdaptadorEvento.ViewHolderData>{

    private IEventoClickListener mEventoClick;

    public List<Evento> listEventos;

    //private SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yyyy");
    //private SimpleDateFormat dh = new SimpleDateFormat("HH:mm aa");

    public RecyclerViewAdaptadorEvento(List<Evento> listEventos, IEventoClickListener eventoClickListener) {
        this.listEventos = listEventos;
        this.mEventoClick = eventoClickListener;
    }

    public Evento getItemAtIndexPosition(int position){
        return listEventos.get(position);
    }

    @NonNull
    @Override
    public ViewHolderData onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_evento,parent,false);
        ViewHolderData viewHolder =  new ViewHolderData(view, mEventoClick);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderData holder, int position) {
        String Lugar = listEventos.get(position).getUbicacion().getCalle() + " - " + listEventos.get(position).getUbicacion().getZona().getDescripcionZona();
        Lugar = Lugar + "\n" + listEventos.get(position).getUbicacion().getZona().getProvicia().getDescripcion() + " ," + listEventos.get(position).getUbicacion().getZona().getProvicia().getPais().getDescripcion();
        holder.NombreEvento.setText(listEventos.get(position).getTitulo());

        String dateUsed = listEventos.get(position).getFecha();
        String[] parts = dateUsed.split("-");
        String year = parts[0];
        String month = parts[1];
        String day = parts[2];

        String nuevaFecha = day + "/" + month + "/" + year;

        holder.Organizador.setText(nuevaFecha);
        holder.FechaLugar.setText(Lugar);
        //holder.DescripcionP.setText(listEventos.get(position).getDescripcion());
        holder.DescripcionP.setText(listEventos.get(position).getCategoria().getDescripcion());
        holder.fotoEvento.setImageResource(R.mipmap.mapa_logo);
    }

    @Override
    public int getItemCount() {
        return listEventos.size();
    }


    public static class ViewHolderData extends RecyclerView.ViewHolder implements View.OnClickListener{

        private IEventoClickListener eventoClickListener;
        private TextView NombreEvento,Organizador,FechaLugar,DescripcionP;
        ImageView fotoEvento;
        private RatingBar rbPuntajeEventoListado;

        public ViewHolderData(@NonNull View itemView, IEventoClickListener _eventoClickListener) {
            super(itemView);
            NombreEvento = (TextView) itemView.findViewById(R.id.txtNombreEvento);
            Organizador = (TextView) itemView.findViewById(R.id.txtOrganizadorEvento);
            FechaLugar = (TextView) itemView.findViewById(R.id.txtFechaLugar);
            DescripcionP = (TextView) itemView.findViewById(R.id.txtDescripcionPeq);
            fotoEvento = (ImageView) itemView.findViewById(R.id.imgEvento);


            this.eventoClickListener = _eventoClickListener;

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            eventoClickListener.onEventoClick(getAdapterPosition());
            eventoClickListener.onEventoDenunciadoClick(getAdapterPosition());
        }

    }

    public interface IEventoClickListener{
        void onEventoClick(int position);
        void onEventoDenunciadoClick(int position);
    }
}
