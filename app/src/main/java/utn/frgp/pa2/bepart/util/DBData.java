package utn.frgp.pa2.bepart.util;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBData {

    //Información de la BD
    //public static String host="btkqow1wqdhshhopkfs4-mysql.services.clever-cloud.com";

    public static String host="bepart.cqdvf8edozep.us-east-2.rds.amazonaws.com";
    public static String port="3306";
    public static String nameBD="bepart";
    public static String user="admin";
    public static String pass="db_bepart_pa2";

    /*public static String host="bthtrmpqxytittudizis-mysql.services.clever-cloud.com";
    public static String port="3306";
    public static String nameBD="bthtrmpqxytittudizis";
    public static String user="ua36botu1wjoyp4d";
    public static String pass="4G1ae8u365iZpB5gP329";*/

    //Información para la conexion
    public static String urlMySQL = "jdbc:mysql://" + host + ":" + port + "/" + nameBD + "?autoReconnect=true&useSSL=false";
    public static String driver = "com.mysql.jdbc.Driver";


    private Connection connection;

    public DBData() {
        try {
            Class.forName(driver);
            this.connection = DriverManager.getConnection(urlMySQL, user, pass);
        } catch (Exception e) {
            e.printStackTrace();
            try {
                connection.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public Connection getConnection() {
        return connection;
    }

}
