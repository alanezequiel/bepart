package utn.frgp.pa2.bepart.tasks;

import android.os.AsyncTask;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import utn.frgp.pa2.bepart.entity.Ubicacion;
import utn.frgp.pa2.bepart.util.DBData;

public class AltaUbicacionTask extends AsyncTask<Void, String, Integer> {

    private static final String QUERY ="{call sp_RegistrarUbicacion(?,?,?,?)}";
    private Ubicacion ubicacion;
    public AltaUbicacionTask(Ubicacion ubicacion){
        this.ubicacion = ubicacion;
    }
    @Override
    public Integer doInBackground(Void... voids) {

        Integer temp = new Integer(0);
        Connection connection = new DBData().getConnection();
        try{

            CallableStatement preparedStatement = connection.prepareCall(QUERY);

            int i = 1;
            preparedStatement.setInt(i++,this.ubicacion.getZona().getIdZona());
            preparedStatement.setString(i++,this.ubicacion.getCalle());
            preparedStatement.setDouble(i++,this.ubicacion.getLatitud());
            preparedStatement.setDouble(i++,this.ubicacion.getLongitud());

            temp = preparedStatement.executeUpdate();
            connection.close();
        }catch(Exception ex){
            ex.printStackTrace();
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return temp;
    }
}
