package utn.frgp.pa2.bepart.Session;

import android.content.Context;

import utn.frgp.pa2.bepart.entity.Ubicacion;

public class UbicacionesSession {

    Session session;

    public UbicacionesSession(Context context){
        session = new Session(context, SessionData.UbicacionesSessionFile);
    }

    public void loadUbicacionToSessionData(Ubicacion ubicacion){
        session.putUbicacionSessionData(SessionData.UbicacionesSessionKey, ubicacion);
    }

    public Ubicacion getUbicacionFromSessionData(){
        return session.getUbicacionSessionData(SessionData.UbicacionesSessionKey);
    }

    public void clearUbicacionFromSession(){
        session.removeUbicacionSessionData(SessionData.UbicacionesSessionKey);
    }
}
