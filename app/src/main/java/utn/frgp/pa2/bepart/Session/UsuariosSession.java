package utn.frgp.pa2.bepart.Session;

import android.content.Context;

import utn.frgp.pa2.bepart.entity.Usuario;

public class UsuariosSession {

    Session session;

    public UsuariosSession(Context context){
        session = new Session(context, SessionData.UsersSessionFile);
    }

    public void loadUserToSessionData(Usuario usuario){
        session.putUsuarioSessionData(SessionData.UsersSessionKey, usuario);
    }

    public Usuario getUserFromSessionData(){
        return session.getUsuarioSessionData(SessionData.UsersSessionKey);
    }

    public void clearUsuarioFromSession(){
        session.removeUsuarioSessionData(SessionData.UsersSessionKey);
    }

    public void logOutUserSession() {
        this.clearUsuarioFromSession();
        session.logOut();
    }

}
