package utn.frgp.pa2.bepart.tasks;

import android.content.Context;
import android.os.AsyncTask;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import utn.frgp.pa2.bepart.entity.Evento;
import utn.frgp.pa2.bepart.entity.Pais;
import utn.frgp.pa2.bepart.entity.Provincia;
import utn.frgp.pa2.bepart.entity.Zona;
import utn.frgp.pa2.bepart.util.DBData;

public class BuscarZonaTask extends AsyncTask<Void, String, ArrayList<Zona>> {
    private static final String QUERY = "{call sp_BuscarZona(?,?)}";
    private int zonaInt;
    private String zona;

    public BuscarZonaTask(int zonaInt, String zona) {
        this.zonaInt = zonaInt;
        this.zona = zona;
    }

    @Override
    public ArrayList<Zona> doInBackground(Void... voids) {
        Zona zona_temp = null;
        ArrayList<Zona> resultado = new ArrayList<>();
        Connection connection = new DBData().getConnection();

        try {
            CallableStatement callableStatement = connection.prepareCall(QUERY);

            int i = 1;

            if(this.zonaInt == 0){
                callableStatement.setInt(i++, 0);
            }else{
                callableStatement.setInt(i++, this.zonaInt);
            }

            if(this.zona.equals("")){
                callableStatement.setInt(i++, 0);
            }else{
                callableStatement.setString(i++, this.zona);
            }


            callableStatement.execute();
            ResultSet resultSet = callableStatement.getResultSet();
            while(resultSet.next()){
                zona_temp = new Zona();
                zona_temp.setIdZona(resultSet.getInt("IdZona"));
                zona_temp.setProvicia(new Provincia(resultSet.getInt("idProvincia"),
                        new Pais(resultSet.getInt("idPais"),resultSet.getString("Pais")),
                        resultSet.getString("Provincia")));
                zona_temp.setDescripcionZona(resultSet.getString("Zona"));
                zona_temp.setCodigoPostal(resultSet.getString("CodigoPostal"));

                resultado.add(zona_temp);
            }
            connection.close();
        }catch(Exception ex){
            ex.printStackTrace();
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return resultado;
    }
}
