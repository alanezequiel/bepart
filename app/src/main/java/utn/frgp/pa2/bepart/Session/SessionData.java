package utn.frgp.pa2.bepart.Session;

public class SessionData {
    public final static String UsersSessionFile = "users_session_file";
    public final static String UsersSessionKey = "users_session_key";

    public final static String UbicacionesSessionFile = "ubicaciones_session_file";
    public final static String UbicacionesSessionKey = "ubicaciones_session_key";

    public final static String DocumentacionSessionFile = "documentacion_session_file";
    public final static String DocumentacionSessionKey = "documentacion_session_key";

    public final static String GeneralSessionFile = "general_session_file";
}
