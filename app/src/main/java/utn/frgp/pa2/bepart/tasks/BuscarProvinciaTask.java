package utn.frgp.pa2.bepart.tasks;

import android.os.AsyncTask;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import utn.frgp.pa2.bepart.entity.Pais;
import utn.frgp.pa2.bepart.entity.Provincia;
import utn.frgp.pa2.bepart.util.DBData;

public class BuscarProvinciaTask extends AsyncTask<Void, String, ArrayList<Provincia>> {

    private static final String QUERY = "{call sp_BuscarProvincia(?,?)}";
    private int provinciaInt;
    private String provincia;

    public BuscarProvinciaTask(int provinciaInt, String provincia) {
        this.provinciaInt = provinciaInt;
        this.provincia = provincia;
    }
    @Override
    public ArrayList<Provincia> doInBackground(Void... voids) {
        Provincia provincia_temp = null;
        ArrayList<Provincia> resultado = new ArrayList<>();
        Connection connection = new DBData().getConnection();

        try {
            CallableStatement callableStatement = connection.prepareCall(QUERY);

            int i = 1;

            if(this.provinciaInt == 0){
                callableStatement.setInt(i++, 0);
            }else{
                callableStatement.setInt(i++, this.provinciaInt);
            }

            if(this.provincia.equals("")){
                callableStatement.setString(i++, "0");
            }else{
                callableStatement.setString(i++, this.provincia);
            }
            callableStatement.execute();
            ResultSet resultSet = callableStatement.getResultSet();
            while(resultSet.next()){
                provincia_temp = new Provincia();
                provincia_temp.setIdProvincia(resultSet.getInt("idProvincia"));
                provincia_temp.setPais(new Pais(resultSet.getInt("idPais"),resultSet.getString("Pais")));
                provincia_temp.setDescripcion(resultSet.getString("Provincia"));

                resultado.add(provincia_temp);
            }
            connection.close();
        }catch(Exception ex){
            ex.printStackTrace();
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return resultado;
    }
}
