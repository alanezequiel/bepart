package utn.frgp.pa2.bepart.model;

import java.util.concurrent.ExecutionException;

import utn.frgp.pa2.bepart.entity.Usuario;
import utn.frgp.pa2.bepart.tasks.AltaUsuarioTask;
import utn.frgp.pa2.bepart.util.IConnectable;

public class UsuariosModel implements IConnectable<Usuario> {

    @Override
    public boolean insert(Usuario obj) throws ExecutionException, InterruptedException {
        if(obj == null){
            return false;
        }
        AltaUsuarioTask altaTask = new AltaUsuarioTask(obj);
        altaTask.execute();
       if(altaTask.get() != 0)
           return true;
        return false;
    }

    @Override
    public boolean update(Usuario obj) throws ExecutionException, InterruptedException {
        return false;
    }
}
