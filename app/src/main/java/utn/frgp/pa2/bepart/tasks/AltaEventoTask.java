package utn.frgp.pa2.bepart.tasks;

import android.os.AsyncTask;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;

import utn.frgp.pa2.bepart.entity.Evento;
import utn.frgp.pa2.bepart.util.DBData;

public class AltaEventoTask extends AsyncTask<Void, String, Integer> {

    private static final String QUERY ="{call sp_RegistrarEvento(?,?,?,?,?,?,?,?,?,?)}";
    private Evento evento;
    private int idUsuarioOrg;
    public AltaEventoTask(Evento evento,int idUsuarioOrg){
        this.evento = evento;
        this.idUsuarioOrg = idUsuarioOrg;
    }
    @Override
    public Integer doInBackground(Void... voids) {

        Integer temp = new Integer(0);
        Connection connection = new DBData().getConnection();
        try{

            CallableStatement preparedStatement = connection.prepareCall(QUERY);

            int i = 1;
            preparedStatement.setString(i++,this.evento.getTitulo());
            preparedStatement.setString(i++,this.evento.getDescripcion());
            preparedStatement.setInt(i++,this.evento.getUbicacion().getIdUbicacion());
            preparedStatement.setString(i++, this.evento.getFecha());
            preparedStatement.setInt(i++, this.evento.getCupos());
            preparedStatement.setString(i++, "defaultevento.png");
            preparedStatement.setInt(i++, this.evento.getCategoria().getIdCategoria());
            preparedStatement.setInt(i++, this.evento.getEstadoEvento().getIdEstadoEvento());
            preparedStatement.setInt(i++, this.idUsuarioOrg);
            preparedStatement.setString(i++, this.evento.getHora());

            temp = preparedStatement.executeUpdate();
            connection.close();
        }catch(Exception ex){
            ex.printStackTrace();
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return temp;
    }
}
