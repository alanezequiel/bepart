package utn.frgp.pa2.bepart.Session;


import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import utn.frgp.pa2.bepart.entity.Documentacion;
import utn.frgp.pa2.bepart.entity.EstadoUsuario;
import utn.frgp.pa2.bepart.entity.TipoUsuario;
import utn.frgp.pa2.bepart.entity.Ubicacion;
import utn.frgp.pa2.bepart.entity.Usuario;


public class Session {
    private SharedPreferences session;
    private SharedPreferences.Editor editor;

    public Session(Context _context, String sessionFile) {
        this.session = _context.getSharedPreferences(sessionFile, Context.MODE_PRIVATE);
        this.editor = session.edit();
    }

    public void putSessionData(String key, String value){
        this.editor.putString(key, value);
        this.editor.apply();
    }

    public void putSessionData(String key, int value){
        this.editor.putInt(key, value);
        this.editor.apply();
    }

    public String getStringSessionData(String key){
        return this.session.getString(key, null);
    }

    //Esto es malisimo... no puedo poner null como default...
    // no puedo distinguir si un -1 es por default o el valor original.
    //Vamos a trabajar solo con positivos...
    public Integer getIntSessionData(String key){
        return this.session.getInt(key, -1);
    }


    public void putUsuarioSessionData(String key, Usuario value){
        final Gson gson = new Gson();
        String serializedObject = gson.toJson(value);
        this.editor.putString(key, serializedObject);
        this.editor.apply();
    }

    public Usuario getUsuarioSessionData(String key){
        final Gson gson = new Gson();
        return gson.fromJson(this.session.getString(key, ""), Usuario.class);
    }

    public void removeUsuarioSessionData(String key){
        this.editor.remove(key);
        this.editor.commit();
    }

    public void putUbicacionSessionData(String key, Ubicacion value){
        final Gson gson = new Gson();
        String serializedObject = gson.toJson(value);
        this.editor.putString(key, serializedObject);
        this.editor.apply();
    }

    public Ubicacion getUbicacionSessionData(String key){
        final Gson gson = new Gson();
        return gson.fromJson(this.session.getString(key, ""), Ubicacion.class);
    }

    public void removeUbicacionSessionData(String key){
        this.editor.remove(key);
        this.editor.commit();
    }

    public void putDocumentacionSessionData(String key, Documentacion value){
        //final Gson gson = new Gson();
        Gson gson = new GsonBuilder().setDateFormat("MMM d, yyyy HH:mm:ss a").create();
        String serializedObject = gson.toJson(value);
        this.editor.putString(key, serializedObject);
        this.editor.apply();
    }

    public Documentacion getDocumentacionSessionData(String key){
        //final Gson gson = new Gson();
        Gson gson = new GsonBuilder().setDateFormat("MMM d, yyyy HH:mm:ss a").create();
        return gson.fromJson(this.session.getString(key, ""), Documentacion.class);
    }

    public void removeDocumentacionSessionData(String key){
        this.editor.remove(key);
        this.editor.commit();
    }


    public void putObjectSessionData(String key, Object value){
        final Gson gson = new Gson();
        String serializedObject = gson.toJson(value);
        this.editor.putString(key, serializedObject);
        this.editor.apply();
    }

    public Object getObjectSessionData(String key, Class ObjectClass){
        final Gson gson = new Gson();
        return gson.fromJson(this.session.getString(key, ""), ObjectClass);
    }

    public void putEstadoUsuarioSessionData(String key, EstadoUsuario value){
        final Gson gson = new Gson();
        String serializedObject = gson.toJson(value);
        this.editor.putString(key, serializedObject);
        this.editor.apply();
    }

    public EstadoUsuario getEstadoUsuarioSessionData(String key){
        final Gson gson = new Gson();
        return gson.fromJson(this.session.getString(key, ""), EstadoUsuario.class);
    }

    public void putTipoUsuarioSessionData(String key, TipoUsuario value){
        final Gson gson = new Gson();
        String serializedObject = gson.toJson(value);
        this.editor.putString(key, serializedObject);
        this.editor.apply();
    }

    public TipoUsuario getTipoUsuarioSessionData(String key){
        final Gson gson = new Gson();
        return gson.fromJson(this.session.getString(key, ""), TipoUsuario.class);
    }

    public void logOut() {
        editor.clear();
        editor.commit();
    }

}
