package utn.frgp.pa2.bepart.dialogos;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.AdaptiveIconDrawable;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import utn.frgp.pa2.bepart.R;
import utn.frgp.pa2.bepart.Session.UbicacionesSession;
import utn.frgp.pa2.bepart.entity.Evento;
import utn.frgp.pa2.bepart.entity.Pais;
import utn.frgp.pa2.bepart.entity.Provincia;
import utn.frgp.pa2.bepart.entity.Ubicacion;
import utn.frgp.pa2.bepart.entity.Zona;
import utn.frgp.pa2.bepart.interfaces.IComunicaFragments;

public class DialogoDireccionEvento extends Fragment implements OnMapReadyCallback, GoogleMap.InfoWindowAdapter, GoogleMap.OnMarkerDragListener {

    private Context contexto;
    private GoogleMap gMap;
    private MapView mapView;
    private SearchView searchView;
    private String idUbicacion = null;
    private Marker marker = null;
    private Address addresses = null;
    private Geocoder geocoder = null;
    List<Address> addressList = null;
    Dialog dialogo;
    boolean flag = false;


    public DialogoDireccionEvento(Context context){
        this.contexto = context;
        dialogo = new Dialog(contexto);
        dialogo.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogo.setCancelable(true);
        dialogo.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialogo.setContentView(R.layout.fragment_ubicacion_evento);

        mapView = (MapView) dialogo.findViewById(R.id.map);
        if (mapView != null){
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this); }
        searchView = (SearchView) dialogo.findViewById(R.id.sv_ubicacion);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                String locacion = searchView.getQuery().toString();

                if (idUbicacion!=null)
                {
                    gMap.clear();
                }
                if(locacion != null || !locacion.equals("")){
                    geocoder = new Geocoder(contexto);
                    try{
                        addressList = geocoder.getFromLocationName(locacion,1);
                    }catch (IOException e) {
                        e.printStackTrace();
                    }
                    addresses = addressList.get(0);
                    LatLng latLng = new LatLng(addresses.getLatitude(),addresses.getLongitude());

                    marker = gMap.addMarker(new MarkerOptions()
                            .position(latLng)
                            .draggable(true)
                            );
                    try {
                        addressList = geocoder.getFromLocation(addresses.getLatitude(),addresses.getLongitude(),1);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (addressList.get(0).getThoroughfare()== null || addressList.get(0).getSubThoroughfare()== null
                            || addressList.get(0).getLocality()== null||  addressList.get(0).getAdminArea()== null ||
                            addressList.get(0).getCountryName()== null || !addressList.get(0).getCountryName().equalsIgnoreCase("Argentina") )
                    {
                        Toast.makeText(contexto, "Ubicacion No Disponible", Toast.LENGTH_SHORT).show();
                        flag = true;
                        marker.setTitle("Ubicacion No Disponible");
                        marker.setSnippet("Ubicacion Fuera de alcanse");
                    }
                    else
                    {
                        String title = addressList.get(0).getThoroughfare() + " " + addressList.get(0).getSubThoroughfare()
                                + " - " + addressList.get(0).getLocality();

                        String snipper = addressList.get(0).getAdminArea() + ", " + addressList.get(0).getCountryName();
                        String valorNulleable = "";
                        if ( addressList.get(0).getPostalCode() != null)
                        {
                            valorNulleable = " - " + addressList.get(0).getPostalCode();
                        }
                        marker.setTitle(title + valorNulleable );
                        marker.setSnippet(snipper);
                        flag = false;
                    }

                    marker.showInfoWindow();
                    idUbicacion = marker.getId();
                    gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,15));

                }

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }

        });

        dialogo.show();
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap = googleMap;
        gMap.setOnMarkerDragListener(this);

        gMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {

                //Valida si se quizo seleccionar una fecha invalida antes de guardar
                if (flag==false){
                    Ubicacion objUbicacion = new Ubicacion();

                    objUbicacion.setCalle(addressList.get(0).getThoroughfare() + " " + addressList.get(0).getSubThoroughfare());
                    objUbicacion.setLatitud(marker.getPosition().latitude);
                    objUbicacion.setLongitud(marker.getPosition().longitude);
                    objUbicacion.setZona(new Zona(0,
                            new Provincia(0,
                                    new Pais(1,addressList.get(0).getCountryName())
                                    ,addressList.get(0).getAdminArea()),
                            //addressList.get(0).getLocality(), Integer.parseInt(addressList.get(0).getPostalCode()))
                            addressList.get(0).getLocality(), addressList.get(0).getPostalCode())
                    );
                    UbicacionesSession ubicacionesSession = new UbicacionesSession(contexto);
                    ubicacionesSession.loadUbicacionToSessionData(objUbicacion);
                    Toast.makeText(contexto, "Ubicacion Seleccionado Correctamente", Toast.LENGTH_SHORT).show();
                    //dialogo.dismiss();
                }
                else {
                    Toast.makeText(contexto, "Ubicacion Invalida", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        double latitud = marker.getPosition().latitude;
        double longitud = marker.getPosition().longitude;

        try {
            addressList = geocoder.getFromLocation(latitud,longitud,1);

            if (addressList.get(0).getThoroughfare() == null || addressList.get(0).getSubThoroughfare()== null
                    || addressList.get(0).getLocality()== null || addressList.get(0).getAdminArea()== null ||
            addressList.get(0).getCountryName()== null || !addressList.get(0).getCountryName().equalsIgnoreCase("Argentina"))
            {
                Toast.makeText(contexto, "Ubicacion No Disponible", Toast.LENGTH_SHORT).show();
                flag = true;
                marker.setTitle("Ubicacion No Disponible");
                marker.setSnippet("Ubicacion fuera de alcanse");
            }
            else
            {
                String title = addressList.get(0).getThoroughfare() + " " + addressList.get(0).getSubThoroughfare()
                        + " - " + addressList.get(0).getLocality()
                        + " - " + addressList.get(0).getPostalCode();
                String snipper = addressList.get(0).getAdminArea() + ", " + addressList.get(0).getCountryName();
                marker.setTitle(title);
                marker.setSnippet(snipper);
                flag = false;

            }

            marker.showInfoWindow();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
