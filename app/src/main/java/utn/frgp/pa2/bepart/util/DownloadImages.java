package utn.frgp.pa2.bepart.util;

import android.app.Activity;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import android.graphics.Bitmap;
import android.util.Base64;
import android.widget.ImageView;

import com.google.api.client.util.IOUtils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

public class DownloadImages extends AsyncTask<Void, Void, Bitmap> {

    String name;
    ImageView imageView;
    Activity activity;

    private static final String SERVICE_URL = ImagesServiceData.get_url;

    public DownloadImages(String name, ImageView imageView, Activity activity){
        this.name = name;
        this.imageView = imageView;
        this.activity = activity;
    }

    @Override
    protected Bitmap doInBackground(Void... voids) {

        String url = SERVICE_URL + "/" + this.name;
        try{
            URL _url = new URL(url);
            URLConnection connection = _url.openConnection();
            connection.setConnectTimeout(1000 * 30);
            connection.setReadTimeout(1000 * 30);
            connection.addRequestProperty("content-type", "text/plain");
            connection.setUseCaches(false);
            connection.connect();

            //int len = connection.getContentLength();
            int len;
            //InputStream in = _url.openStream();
            InputStream in = connection.getInputStream();
            StringBuilder sb = new StringBuilder();
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String read;
            while ((read=br.readLine()) != null) {
                sb.append(read);
            }
            br.close();

            byte[] decodedString = Base64.decode(sb.toString(), Base64.DEFAULT);
            Bitmap resp = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

            //Bitmap resp = BitmapFactory.decodeStream((InputStream) connection.getContent(), null, null);
            //Bitmap resp = BitmapFactory.decodeStream((InputStream) _url.getContent(), null, null);
            //Bitmap resp = BitmapFactory.decodeStream((InputStream) _url.openStream(), null, null);

            return resp;

        }catch (Exception e){
            e.printStackTrace();
            return null;
        }

    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
        if(bitmap != null){
            this.imageView.setImageBitmap(bitmap);
        }
    }

}
