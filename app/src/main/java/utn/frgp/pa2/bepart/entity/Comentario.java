package utn.frgp.pa2.bepart.entity;

import java.io.Serializable;

public class Comentario implements Serializable {

    private String NombreUsuario;
    private String Comentario;
    private int puntaje;

    public Comentario(){

    }

    public Comentario(String nombreUsuario, String comentario, int _puntaje) {
        NombreUsuario = nombreUsuario;
        Comentario = comentario;
        puntaje = _puntaje;
    }

    public String getNombreUsuario() {
        return NombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        NombreUsuario = nombreUsuario;
    }

    public String getComentario() {
        return Comentario;
    }

    public void setComentario(String comentario) {
        Comentario = comentario;
    }

    public int getPuntaje() {
        return puntaje;
    }

    public void setPuntaje(int _puntaje) {
        puntaje = _puntaje;
    }
}
