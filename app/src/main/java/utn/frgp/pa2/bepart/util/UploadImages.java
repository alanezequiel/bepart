package utn.frgp.pa2.bepart.util;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Base64;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;


public class UploadImages extends AsyncTask<Void, Void, Void> {

    Bitmap image;
    String name;
    private static final String SERVICE_URL = ImagesServiceData.save_url;

    Activity activity;

    public UploadImages(Bitmap image, String name, Activity activity){
        this.image = image;
        this.name = name;
        this.activity = activity;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
        String encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);

        HttpClient httpClient = new DefaultHttpClient();
        JSONObject dataToSend = new JSONObject();

        HttpPost post = new HttpPost(SERVICE_URL);
        try {
            dataToSend.put("image", encodedImage);
            dataToSend.put("name", this.name);
            post.addHeader("content-type", "application/json");
            StringEntity params = new StringEntity(dataToSend.toString());
            post.setEntity(params);
            httpClient.execute(post);
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
    }

    private HttpParams getHttpRequestParams(){
        HttpParams httpRequestParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpRequestParams, 1000*30);
        HttpConnectionParams.setSoTimeout(httpRequestParams, 1000*30);
        return httpRequestParams;
    }

}
