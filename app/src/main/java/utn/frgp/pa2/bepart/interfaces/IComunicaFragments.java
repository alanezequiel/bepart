package utn.frgp.pa2.bepart.interfaces;

import java.util.ArrayList;

import utn.frgp.pa2.bepart.entity.Comentario;
import utn.frgp.pa2.bepart.entity.Evento;

public interface IComunicaFragments {

    void MostrarDetalleEvento(Evento eventoSeleccionado, ArrayList<Comentario> comentarios);
    void MostrarDatosEdicionEvento(Evento eventoSeleccionado);
    void MostrarDetalleEventoDenunciado(Evento eventoSeleccionado);
}
