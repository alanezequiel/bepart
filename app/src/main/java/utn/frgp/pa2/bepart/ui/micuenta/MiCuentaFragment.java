package utn.frgp.pa2.bepart.ui.micuenta;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.regex.Pattern;

import utn.frgp.pa2.bepart.MainActivity;
import utn.frgp.pa2.bepart.R;
import utn.frgp.pa2.bepart.Session.UsuariosSession;
import utn.frgp.pa2.bepart.entity.Usuario;
import utn.frgp.pa2.bepart.tasks.ActualizarUsuarioTask;
import utn.frgp.pa2.bepart.tasks.BuscarUsuarioTask;
import utn.frgp.pa2.bepart.ui.registro.RegistroFragment;
import utn.frgp.pa2.bepart.ui.splashscreen.splashscreen;

public class MiCuentaFragment extends Fragment {

    private UsuariosSession usuariosSession;
    private Usuario ssUser;


    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (container != null) {
            container.removeAllViews();
        }

        View root = inflater.inflate(R.layout.fragment_micuenta, container, false);
        usuariosSession = new UsuariosSession(getContext());
        ssUser =  usuariosSession.getUserFromSessionData();

        TextView tvNombre = (TextView) root.findViewById(R.id.tvNombremc);
        TextView tvApellido = (TextView) root.findViewById(R.id.tvApellidomc);
        TextView tvPassword = (TextView) root.findViewById(R.id.tvPasswordmc);
        TextView tvUsername = (TextView) root.findViewById(R.id.tvUsernamemc);
        TextView tvEmail = (TextView) root.findViewById(R.id.tvEmailmc);
        TextView txtTipoUser = (TextView) root.findViewById(R.id.txtTipoUser);
        Button btnModificar = (Button) root.findViewById(R.id.btnActualizar);
        Button btnVolver = (Button) root.findViewById(R.id.btnVolver);

        ArrayList<Usuario> resultado = null;
        BuscarUsuarioTask buscarUsuarioTask = new BuscarUsuarioTask(ssUser);
        buscarUsuarioTask.execute();
        try {
            resultado = buscarUsuarioTask.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if( resultado.size() > 0 ){
            tvNombre.setText(ssUser.getNombre());
            tvApellido.setText(ssUser.getApellido());
            tvPassword.setText(ssUser.getPassword());
            tvUsername.setText(ssUser.getUsername());
            tvEmail.setText(ssUser.getEmail());
            txtTipoUser.setText(ssUser.getTipoUsuario().getDescripcion());
        }
        else{
            Toast.makeText(getContext(),"Fallo de Sistema , Intente mas Tarde!",Toast.LENGTH_LONG).show();

        }


        btnModificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String usernameText = tvUsername.getText().toString();
                String nombreText = tvNombre.getText().toString();
                String apellidoText = tvApellido.getText().toString();
                String emailText = tvEmail.getText().toString();
                String passwordText = tvPassword.getText().toString();


                if( usernameText.isEmpty() ||nombreText.isEmpty() || apellidoText.isEmpty()
                        || emailText.isEmpty() || passwordText.isEmpty()){
                    Toast.makeText(getContext(),"No puede haber campos vacios",Toast.LENGTH_LONG).show();
                    return;
                }

                ssUser.setNombre(tvNombre.getText().toString());
                ssUser.setApellido(tvApellido.getText().toString());
                ssUser.setPassword(tvPassword.getText().toString());
                ssUser.setUsername(tvUsername.getText().toString());

                ActualizarUsuarioTask actualizarUsuarioTask = new ActualizarUsuarioTask(ssUser);
                actualizarUsuarioTask.execute();
                try {
                    if(actualizarUsuarioTask.get() != 0) {
                        usuariosSession.loadUserToSessionData(ssUser);
                        Toast.makeText(getContext(), "Datos Grabados Correctamente", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(getContext(), "Username Ya Existente", Toast.LENGTH_SHORT).show();
                    }
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), MainActivity.class);
                startActivity(i);
            }
        });

        return root;
    }

    private boolean validarEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }


}
