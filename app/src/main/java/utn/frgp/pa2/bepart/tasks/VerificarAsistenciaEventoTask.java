package utn.frgp.pa2.bepart.tasks;

import android.os.AsyncTask;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import utn.frgp.pa2.bepart.entity.Categoria;

import utn.frgp.pa2.bepart.util.DBData;

public class VerificarAsistenciaEventoTask extends AsyncTask<Void, String, Boolean> {
    private static final String QUERY = "{call sp_VerificarAsistenciaUsuarioEvento(?,?)}";
    private int idEvento;
    private int idUsuario;

    public VerificarAsistenciaEventoTask(int idEvento, int idUsuario){
        this.idEvento = idEvento;
        this.idUsuario = idUsuario;
    }

    @Override
    public Boolean doInBackground(Void... voids) {
        int resultado = 0;
        Connection connection = new DBData().getConnection();
        try {
            CallableStatement callableStatement = connection.prepareCall(QUERY);
            int i = 1;

            callableStatement.setInt(i++, this.idEvento);
            callableStatement.setInt(i++, this.idUsuario);

            callableStatement.execute();
            ResultSet resultSet = callableStatement.getResultSet();
            while(resultSet.next()){
                resultado = resultSet.getInt("resultado");

            }
            connection.close();
        }catch(Exception ex){
            ex.printStackTrace();
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return (resultado == 1);
    }
}
