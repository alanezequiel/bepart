package utn.frgp.pa2.bepart.ui.moderador;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import java.util.concurrent.ExecutionException;

import utn.frgp.pa2.bepart.R;
import utn.frgp.pa2.bepart.Session.DocumentacionSession;
import utn.frgp.pa2.bepart.Session.Session;
import utn.frgp.pa2.bepart.Session.SessionData;
import utn.frgp.pa2.bepart.entity.Documentacion;
import utn.frgp.pa2.bepart.model.DocumentacionModel;
import utn.frgp.pa2.bepart.ui.eventos.DenunciarEvento;
import utn.frgp.pa2.bepart.util.DownloadImages;

public class RevisionSolicitudOrganizador extends DialogFragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_revisar_solicitud_organizador, container, false);


        TextView tipoDocumento = view.findViewById(R.id.tipo_documento);
        TextView nroDocumento = view.findViewById(R.id.nro_documento);
        ImageView imgDNIFrente = view.findViewById(R.id.frente);
        ImageView imgDNIDorso = view.findViewById(R.id.reverso);
        ImageView imgDNISelfie = view.findViewById(R.id.selfie);
        Button aprobar = view.findViewById(R.id.aprobar);
        Button rechazar = view.findViewById(R.id.rechazar);

        DocumentacionSession docSession = new DocumentacionSession(getContext());
        Documentacion documentacion = new Documentacion();
        documentacion = docSession.getDocumentacionFromSessionData();

        new DownloadImages(documentacion.getImgDNIFrente(), imgDNIFrente, getActivity()).execute();
        new DownloadImages(documentacion.getImgDNIDorso(), imgDNIDorso, getActivity()).execute();
        new DownloadImages(documentacion.getImgSelfie(), imgDNISelfie, getActivity()).execute();

        tipoDocumento.setText(documentacion.getTipo_documento());
        nroDocumento.setText(documentacion.getNroDoc());

        aprobar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConfirmarAprobacion confirmarAprobacion = new ConfirmarAprobacion();
                confirmarAprobacion.show(getActivity().getSupportFragmentManager(), "ConfirmarAprobacion");
                //getDialog().dismiss();
            }
        });

        rechazar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConfirmarRechazo confirmarRechazo = new ConfirmarRechazo();
                confirmarRechazo.show(getActivity().getSupportFragmentManager(), "ConfirmarRechazo");
                //getDialog().dismiss();
            }
        });

        return view;
    }
}
