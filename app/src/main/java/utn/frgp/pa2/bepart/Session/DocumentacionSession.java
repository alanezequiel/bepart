package utn.frgp.pa2.bepart.Session;

import android.content.Context;

import utn.frgp.pa2.bepart.entity.Documentacion;

public class DocumentacionSession {

    Session session;

    public DocumentacionSession(Context context){
        session = new Session(context, SessionData.DocumentacionSessionFile);
    }

    public void loadDocumentacionToSessionData(Documentacion documentacion){
        session.putDocumentacionSessionData(SessionData.DocumentacionSessionKey, documentacion);
    }

    public Documentacion getDocumentacionFromSessionData(){
        return session.getDocumentacionSessionData(SessionData.DocumentacionSessionKey);
    }

    public void clearDocumentacionFromSession(){
        session.removeDocumentacionSessionData(SessionData.DocumentacionSessionKey);
    }

}
