package utn.frgp.pa2.bepart.tasks;

import android.os.AsyncTask;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import utn.frgp.pa2.bepart.entity.Categoria;
import utn.frgp.pa2.bepart.entity.DenunciaxEvento;
import utn.frgp.pa2.bepart.entity.Usuario;
import utn.frgp.pa2.bepart.util.DBData;

public class BuscarDetalleDenunciaTask extends AsyncTask<Void, String, ArrayList<DenunciaxEvento>> {
    private static final String QUERY = "{call sp_BuscarDetalleDenuncia(?)}";
    private int idEvento;

    public BuscarDetalleDenunciaTask(int idEvento) {
        this.idEvento = idEvento;
    }

    @Override
    protected ArrayList<DenunciaxEvento> doInBackground(Void... voids) {
        DenunciaxEvento denuncia_temp = null;
        ArrayList<DenunciaxEvento> resultado = new ArrayList<>();
        Connection connection = new DBData().getConnection();

        try {

            CallableStatement callableStatement = connection.prepareCall(QUERY);

            int i = 1;

            callableStatement.setInt(i++, this.idEvento);

            callableStatement.execute();
            ResultSet resultSet = callableStatement.getResultSet();
            while(resultSet.next()){
                denuncia_temp = new DenunciaxEvento();
                denuncia_temp.setIdDenuncia(resultSet.getInt("IdDenuncia"));
                denuncia_temp.setIdEvento(resultSet.getInt("IdEvento"));
                denuncia_temp.setDenunciante(resultSet.getString("Denunciante"));
                denuncia_temp.setTipoDenuncia(resultSet.getString("TipoDenuncia"));
                denuncia_temp.setComentario(resultSet.getString("Comentario"));
                denuncia_temp.setCantidad(resultSet.getInt("cDenuncias"));
                denuncia_temp.setCuposReales(resultSet.getInt("CuposReales"));

                resultado.add(denuncia_temp);
            }
            connection.close();
        }catch(Exception ex){
            ex.printStackTrace();
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return resultado;
    }

}

