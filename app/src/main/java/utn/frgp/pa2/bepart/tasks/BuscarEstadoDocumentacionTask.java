package utn.frgp.pa2.bepart.tasks;

import android.os.AsyncTask;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import utn.frgp.pa2.bepart.entity.EstadoDocumentacion;
import utn.frgp.pa2.bepart.util.DBData;

public class BuscarEstadoDocumentacionTask extends AsyncTask<Void, String, ArrayList<EstadoDocumentacion>> {
    private static final String QUERY = "{call sp_BuscarEstadoDocumentacion(?)}";
    EstadoDocumentacion estadoDocumentacion;

    public BuscarEstadoDocumentacionTask(EstadoDocumentacion estadoDocumentacion){
        this.estadoDocumentacion = estadoDocumentacion;
    }

    @Override
    public ArrayList<EstadoDocumentacion> doInBackground(Void... voids) {
        EstadoDocumentacion estadoDocumentacion = null;
        ArrayList<EstadoDocumentacion> resultado = new ArrayList<>();
        Connection connection = new DBData().getConnection();

        try {

            CallableStatement callableStatement = connection.prepareCall(QUERY);

            int i = 1;

            callableStatement.setInt(i++, this.estadoDocumentacion.getIdEstadoDoc());

            callableStatement.execute();
            ResultSet resultSet = callableStatement.getResultSet();
            while(resultSet.next()){
                estadoDocumentacion = new EstadoDocumentacion();
                estadoDocumentacion.setIdEstadoDoc(resultSet.getInt("idEstadoDoc"));
                estadoDocumentacion.setDescripcion(resultSet.getString("Descripcion"));
                estadoDocumentacion.setEstado(resultSet.getInt("Estado"));
                resultado.add(estadoDocumentacion);
            }
            connection.close();
        }catch(Exception ex){
            ex.printStackTrace();
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return resultado;
    }
}
