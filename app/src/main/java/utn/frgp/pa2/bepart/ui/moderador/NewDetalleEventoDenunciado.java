package utn.frgp.pa2.bepart.ui.moderador;

import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import utn.frgp.pa2.bepart.R;
import utn.frgp.pa2.bepart.entity.DenunciaxEvento;
import utn.frgp.pa2.bepart.entity.Documentacion;
import utn.frgp.pa2.bepart.entity.Evento;
import utn.frgp.pa2.bepart.tasks.ActualizarEstadoEventoTask;
import utn.frgp.pa2.bepart.tasks.BuscarDetalleDenunciaTask;
import utn.frgp.pa2.bepart.tasks.BuscarEventosTask;

public class NewDetalleEventoDenunciado extends DialogFragment {


    private Evento eventoSeleccionado;
    private IEventoClickListener listener;
    private ArrayAdapter<String> monthAdapter2;
    private int position;

    public interface IEventoClickListener{
        //void onDarBajaEvento(ArrayAdapter<String> monthAdapter);
        void onDarBajaEvento(int position);
    }

    public NewDetalleEventoDenunciado(Evento eventoSeleccionado, IEventoClickListener _listener, int _position, ArrayAdapter<String> _monthAdapter) {
        this.eventoSeleccionado = eventoSeleccionado;
        this.listener = _listener;
        this.position = _position;
        this.monthAdapter2 = _monthAdapter;
    }
    private int estado;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        estado = 0;
        View root = inflater.inflate(R.layout.fragment_new_detalle_evento_denunciado, container, false);
        Button btnBajaEvento = (Button) root.findViewById(R.id.btnBajaEvento);
        TextView tvTitulo = (TextView) root.findViewById(R.id.tvTitulo);
        TextView tvDetalle = (TextView) root.findViewById(R.id.tvDetalle);
        TextView tvLugar = (TextView) root.findViewById(R.id.tvLugar);
        TextView tvFechayHora = (TextView) root.findViewById(R.id.tvFechayHora);
        TextView tvCupos = (TextView) root.findViewById(R.id.tvCupoEvento);
        TextView tvCuposReales = (TextView) root.findViewById(R.id.tvCuposReales);
        TextView tvCategoria = (TextView) root.findViewById(R.id.tvCategoria);
        TextView tvOrganizador = (TextView) root.findViewById(R.id.tvOrganizador);
        TextView tvDenuncias = (TextView) root.findViewById(R.id.tvDenuncias);
        TextView tvParticipante = (TextView) root.findViewById(R.id.tvParticipantes);
        TextView tvPorcentaje = (TextView) root.findViewById(R.id.tvPorcentaje);
        ListView listView = root.findViewById(R.id.lstComentarios);

        String Lugar = obtenerLugar();
        String nuevaFecha = obtenerFecha();
        String nuevaHora = obtenerHora();


        tvTitulo.setText(eventoSeleccionado.getTitulo());
        tvDetalle.setText(eventoSeleccionado.getDescripcion());
        tvLugar.setText(Lugar);
        tvFechayHora.setText(nuevaFecha + " " + nuevaHora + " hs");
        tvCupos.setText(String.valueOf(eventoSeleccionado.getCupos()));
        tvCategoria.setText(eventoSeleccionado.getCategoria().getDescripcion());
        tvOrganizador.setText(eventoSeleccionado.getUsuarioOrg());
        tvDenuncias.setText("0");
        tvParticipante.setText("0");
        tvPorcentaje.setText("0%");

        List<DenunciaxEvento> denuncias = new ArrayList<>();
        ArrayList<DenunciaxEvento> resultado = null;
        BuscarDetalleDenunciaTask buscarDetalleDenunciaTask = new BuscarDetalleDenunciaTask(eventoSeleccionado.getIdEvento());
        buscarDetalleDenunciaTask.execute();
        try {
            resultado = buscarDetalleDenunciaTask.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        for (DenunciaxEvento _denevento : resultado) {
            denuncias.add(_denevento);
        }

        String[] pendientes = new String[denuncias.size()];
        for(int i = 0; i < denuncias.size(); i++){
            pendientes[i] =
                    "Denuncia Por "+  denuncias.get(i).getTipoDenuncia()  + "\n " +
                    "Usuario @" + denuncias.get(i).getDenunciante()  +" dice : \n   '" +
                    denuncias.get(i).getComentario()  + "' \n";
        }

        int participantes = denuncias.get(0).getCuposReales() - eventoSeleccionado.getCupos();
        tvDenuncias.setText(String.valueOf(denuncias.get(0).getCantidad()));
        tvParticipante.setText(String.valueOf(participantes));
        tvCuposReales.setText(String.valueOf(denuncias.get(0).getCuposReales()));

        DecimalFormat format = new DecimalFormat("#.##");
        double porcentaje = denuncias.get(0).getCantidad() * 100;
        porcentaje = porcentaje / denuncias.get(0).getCuposReales();
        tvPorcentaje.setText(format.format(porcentaje) + "%");

        ArrayAdapter<String> monthAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, pendientes);
        listView.setAdapter(monthAdapter);



        btnBajaEvento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(estado == 0) {
                    ActualizarEstadoEventoTask actualizarEstadoEventoTask = new ActualizarEstadoEventoTask(eventoSeleccionado.getIdEvento(), 0);
                    actualizarEstadoEventoTask.execute();
                    //listView.removeAllViews();
                    Toast.makeText(getContext(), "Evento dado de Baja Correctamente", Toast.LENGTH_SHORT).show();
                    String objeto = monthAdapter2.getItem(position);
                    ArrayAdapter<String> monthAdapter3;

                    //monthAdapter2.remove(objeto);
                    listener.onDarBajaEvento(eventoSeleccionado.getIdEvento());
                    NewDetalleEventoDenunciado.this.dismiss();
                    estado++;
                }else{
                    Toast.makeText(getContext(), "Este evento ya esta dado de Baja", Toast.LENGTH_SHORT).show();
                }
            }
        });


        return root;
    }

    public String obtenerLugar(){
        String Lugar = eventoSeleccionado.getUbicacion().getCalle() + " - " + eventoSeleccionado.getUbicacion().getZona().getDescripcionZona();
        if(eventoSeleccionado.getUbicacion().getZona().getCodigoPostal() != null){
            Lugar = Lugar + "(" + eventoSeleccionado.getUbicacion().getZona().getCodigoPostal() + ")";
        }
        Lugar = Lugar + "\n" + eventoSeleccionado.getUbicacion().getZona().getProvicia().getDescripcion() + " ," + eventoSeleccionado.getUbicacion().getZona().getProvicia().getPais().getDescripcion();

        return Lugar;
    }

    public String obtenerFecha(){
        String dateUsed = eventoSeleccionado.getFecha();
        String[] parts = dateUsed.split("-");
        String year = parts[0];
        String month = parts[1];
        String day = parts[2];

        String nuevaFecha = day + "/" + month + "/" + year;

        return nuevaFecha;
    }

    public String obtenerHora(){
        String timeUsed = eventoSeleccionado.getHora();
        String[] partTime = timeUsed.split(":");
        String hour = partTime[0];
        String minute = partTime[1];
        String second = partTime[2];

        String nuevaHora = hour + ":" + minute;

        return nuevaHora;
    }
}