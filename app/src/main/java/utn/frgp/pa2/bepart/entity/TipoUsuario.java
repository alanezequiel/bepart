package utn.frgp.pa2.bepart.entity;

import java.io.Serializable;

public class TipoUsuario implements Serializable {
    private  int idTipoUsuario;
    private String descripcion;

    public static final int USUARIO = 1;
    public static final int ORGANIZADOR = 2;
    public static final int MODERADOR = 3;

    public TipoUsuario() {

    }

    public TipoUsuario(int idTipoUsuario, String descripcion) {
        this.idTipoUsuario = idTipoUsuario;
        this.descripcion = descripcion;
    }

    public int getIdTipoUsuario() {
        return idTipoUsuario;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setIdTipoUsuario(int idTipoUsuario) {
        this.idTipoUsuario = idTipoUsuario;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
