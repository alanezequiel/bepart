package utn.frgp.pa2.bepart.tasks;

import android.os.AsyncTask;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import utn.frgp.pa2.bepart.entity.Zona;
import utn.frgp.pa2.bepart.util.DBData;

public class AltaZonaTask extends AsyncTask<Void, String, Integer> {

    private static final String QUERY ="{call sp_RegistrarZona(?,?,?,?)}";
    private Zona zona;
    public AltaZonaTask(Zona zona){ this.zona = zona; }
    @Override
    public Integer doInBackground(Void... voids) {

        Integer temp = new Integer(0);
        Connection connection = new DBData().getConnection();
        try{

            CallableStatement preparedStatement = connection.prepareCall(QUERY);

            int i = 1;
            preparedStatement.setInt(i++,this.zona.getProvicia().getIdProvincia());
            preparedStatement.setString(i++,this.zona.getDescripcionZona());
            preparedStatement.setString(i++,this.zona.getCodigoPostal());
            preparedStatement.setInt(i++,1); //El estado

            temp = preparedStatement.executeUpdate();
            connection.close();
        }catch(Exception ex){
            ex.printStackTrace();
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return temp;
    }
}
