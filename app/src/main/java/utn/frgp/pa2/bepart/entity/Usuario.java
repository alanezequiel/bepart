package utn.frgp.pa2.bepart.entity;

import java.io.Serializable;

public class Usuario implements Serializable {
    private int idUsuario;
    private String username;
    private String nombre;
    private String apellido;
    private String razonSocial;
    private String email;
    private String password;
    private EstadoUsuario estadoUsuario;
    private int activo;
    private TipoUsuario tipoUsuario;

    public Usuario() {

    }

    public Usuario(String username, String nombre, String apellido, String razonSocial, String email,
                   String password, EstadoUsuario estadoUsuario, int activo, TipoUsuario tipoUsuario ) {
        this.username = username;
        this.nombre = nombre;
        this.apellido = apellido;
        this.razonSocial = razonSocial;
        this.email = email;
        this.password = password;
        this.estadoUsuario = estadoUsuario;
        this.activo = activo;
        this.tipoUsuario = tipoUsuario;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public String getUsername() {
        return username;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public EstadoUsuario getEstadoUsuario() {
        return estadoUsuario;
    }

    public int getActivo() {
        return activo;
    }

    public TipoUsuario getTipoUsuario() {
        return tipoUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String contrasenia) {
        this.password = contrasenia;
    }

    public void setEstadoUsuario(EstadoUsuario estadoUsuario) {
        this.estadoUsuario = estadoUsuario;
    }
    public void setActivo(int activo) {
        this.activo = activo;
    }

    public void setTipoUsuario(TipoUsuario tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }
}

