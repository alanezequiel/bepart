package utn.frgp.pa2.bepart.tasks;

import android.os.AsyncTask;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import utn.frgp.pa2.bepart.entity.Usuario;
import utn.frgp.pa2.bepart.util.DBData;

public class ActualizarEstadoEventoTask extends AsyncTask<Void, String, Integer> {

    private static final String QUERY ="{call sp_ActualizarEstadoEvento(?,?)}";
    private int idEvento;
    private int idEstado;

    public ActualizarEstadoEventoTask(int idEvento,int idEstado){
        this.idEvento = idEvento;
        this.idEstado = idEstado;
    }

    @Override
    protected Integer doInBackground(Void... voids) {
        Integer temp = new Integer(0);
        Connection connection = new DBData().getConnection();
        try{

            CallableStatement preparedStatement = connection.prepareCall(QUERY);

            int i = 1;
            preparedStatement.setInt(i++,this.idEvento);
            preparedStatement.setInt(i++,this.idEstado);
            temp = preparedStatement.executeUpdate();
            connection.close();
        }catch(Exception ex){
            ex.printStackTrace();
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return temp;
    }
}
