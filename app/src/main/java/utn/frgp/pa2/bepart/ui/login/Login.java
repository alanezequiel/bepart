package utn.frgp.pa2.bepart.ui.login;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import utn.frgp.pa2.bepart.MainActivity;
import utn.frgp.pa2.bepart.R;
import utn.frgp.pa2.bepart.Session.UsuariosSession;
import utn.frgp.pa2.bepart.entity.Usuario;
import utn.frgp.pa2.bepart.tasks.BuscarUsuarioTask;
import utn.frgp.pa2.bepart.ui.registro.RegistroFragment;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Login#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Login extends Fragment {

    private static final String TAG = "Login";


    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public Login() {
        // Required empty public constructor
    }

    public static Login newInstance(String param1, String param2) {
        Login fragment = new Login();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                //nada.
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);
        UsuariosSession usuariosSession = new UsuariosSession(getContext());
        Usuario ssUser =  usuariosSession.getUserFromSessionData();

        if(ssUser != null){
            usuariosSession.logOutUserSession();
            Log.e(TAG, "onCreate: Sesion cerrada!");
        }

        if (getActivity().getActionBar() != null) {
            ActionBar actionBar = getActivity().getActionBar();
            actionBar.setDisplayHomeAsUpEnabled(false);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_login, container, false);
        Button btnLogin = root.findViewById(R.id.btnLogin);
        TextView tvRegistrarme = root.findViewById(R.id.tvRegistrarme);


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText email = (EditText) root.findViewById(R.id.edtEmailLogin);
                EditText passwd = (EditText) root.findViewById(R.id.edtPassswordLogin);

                Usuario usuario = new Usuario();
                usuario.setEmail(email.getText().toString());

                ArrayList<Usuario> resultado = null;
                BuscarUsuarioTask buscarUsuarioTask = new BuscarUsuarioTask(usuario);
                buscarUsuarioTask.execute();
                try {
                    resultado = buscarUsuarioTask.get();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if( resultado.size() > 0 && resultado.get(0).getPassword().equals(passwd.getText().toString())){
                    UsuariosSession usuariosSession = new UsuariosSession(getActivity());
                    usuariosSession.loadUserToSessionData(resultado.get(0));

                    Intent i = new Intent(getActivity(), MainActivity.class);
                    startActivity(i);
                }
                else{
                    Toast.makeText(getContext(),"Usuario o Email Incorrecto",Toast.LENGTH_LONG).show();
                    email.setText("");
                    passwd.setText("");
                }
            }
        });

        tvRegistrarme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction().replace(
                        R.id.nav_host_fragment,new RegistroFragment()).setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).commit();
            }
        });

        return root;
    }

}
