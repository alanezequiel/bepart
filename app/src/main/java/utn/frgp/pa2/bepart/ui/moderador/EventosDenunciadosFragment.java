package utn.frgp.pa2.bepart.ui.moderador;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import utn.frgp.pa2.bepart.R;
import utn.frgp.pa2.bepart.Session.DocumentacionSession;
import utn.frgp.pa2.bepart.entity.Categoria;
import utn.frgp.pa2.bepart.entity.Documentacion;
import utn.frgp.pa2.bepart.entity.EstadoDocumentacion;
import utn.frgp.pa2.bepart.entity.Evento;
import utn.frgp.pa2.bepart.entity.Provincia;
import utn.frgp.pa2.bepart.entity.Zona;
import utn.frgp.pa2.bepart.interfaces.IComunicaFragments;
import utn.frgp.pa2.bepart.model.DocumentacionModel;
import utn.frgp.pa2.bepart.recyclerview.RecyclerViewAdaptadorEvento;
import utn.frgp.pa2.bepart.tasks.BuscarCategoriaTask;
import utn.frgp.pa2.bepart.tasks.BuscarEventosTask;
import utn.frgp.pa2.bepart.tasks.BuscarZonaTask;

public class EventosDenunciadosFragment extends Fragment implements RecyclerViewAdaptadorEvento.IEventoClickListener{

    private RecyclerView recyclerViewEvento;
    private RecyclerViewAdaptadorEvento adaptadorEvento;
    private Spinner spnZona, spnCategoria;
    private Button imgBuscar;
    private Activity activity;
    private IComunicaFragments mComunicaFragments;


    public EventosDenunciadosFragment(){

    }

    public static EventosDenunciadosFragment newInstance(){
        return new EventosDenunciadosFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (container != null) {
            container.removeAllViews();
        }

        View contenedorView = inflater.inflate(R.layout.fragment_explorar_lupa, container, false);

        recyclerViewEvento = contenedorView.findViewById(R.id.rvItemEventos);
        recyclerViewEvento.setLayoutManager(new LinearLayoutManager(getContext()));
        spnZona = (Spinner) contenedorView.findViewById(R.id.spnZona);
        spnCategoria = (Spinner) contenedorView.findViewById(R.id.spnCategoria);
        imgBuscar = (Button) contenedorView.findViewById(R.id.imgBuscar);

        ArrayList<Zona> listaZona = new ArrayList<Zona>();
        try {
            listaZona = obtenerZonas();
            ArrayAdapter<Zona> adpZona = new ArrayAdapter<Zona>(getContext(), android.R.layout.simple_spinner_item, listaZona);
            adpZona.setDropDownViewResource(android.R.layout.simple_list_item_checked);
            spnZona.setAdapter(adpZona);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ArrayList<Categoria> listaCategoria = new ArrayList<Categoria>();
        try {
            listaCategoria = obtenerCategorias();
            ArrayAdapter<Categoria> adpCategoria = new ArrayAdapter<Categoria>(getContext(), android.R.layout.simple_spinner_item,listaCategoria);
            adpCategoria.setDropDownViewResource(android.R.layout.simple_list_item_checked);
            spnCategoria.setAdapter(adpCategoria);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        imgBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Categoria cs= (Categoria) spnCategoria.getSelectedItem();
                Zona zs = (Zona) spnZona.getSelectedItem();
                Toast.makeText(getContext(), "Zona : " + zs.getDescripcionZona() + " Categoria : " + cs.getDescripcion(), Toast.LENGTH_SHORT).show();
                try {
                    cargarEventos(cs.getIdCategoria(),zs.getIdZona());
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        try {
            cargarEventos(0,0); //(0,0) = TODOS LOS EVENTOS
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        return contenedorView;

    }

    public void cargarEventos(int idCategoria,int idZona) throws ExecutionException, InterruptedException {
        adaptadorEvento = new RecyclerViewAdaptadorEvento(obtenerEventos(idCategoria,idZona), this);
        recyclerViewEvento.setAdapter(adaptadorEvento);
    }

    //OBTENGO LOS EVENTOS (0,0) TODOS
    public List<Evento> obtenerEventos(int idCategoria, int idZona) throws ExecutionException, InterruptedException {
        List <Evento> evento = new ArrayList<>();
        ArrayList<Evento> resultado = null;
        BuscarEventosTask buscarEventosTask = new BuscarEventosTask(idCategoria,idZona,getContext());
        buscarEventosTask.execute();
        resultado = buscarEventosTask.get();

        for (Evento _evento : resultado) {
            evento.add(_evento);
        }

        return evento;
    }

    public ArrayList<Categoria> obtenerCategorias() throws ExecutionException, InterruptedException {
        ArrayList<Categoria> categoria = new ArrayList<>();
        ArrayList<Categoria> resultado = null;
        BuscarCategoriaTask buscarCategoriaTask = new BuscarCategoriaTask(0);
        buscarCategoriaTask.execute();
        resultado = buscarCategoriaTask.get();

        categoria.add(new Categoria(0,"Seleccionar"));

        for (Categoria _categoria : resultado) {
            categoria.add(_categoria);
        }

        return categoria;
    }

    public ArrayList<Zona> obtenerZonas() throws ExecutionException, InterruptedException {
        ArrayList<Zona> zona = new ArrayList<>();
        ArrayList<Zona> resultado = null;
        BuscarZonaTask buscarZonaTask = new BuscarZonaTask(0,"0");
        buscarZonaTask.execute();
        resultado = buscarZonaTask.get();

        zona.add(new Zona(0,new Provincia(),"Seleccionar","0"));

        for (Zona _zona : resultado) {
            zona.add(_zona);
        }

        return zona;
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if(context instanceof Activity){
            activity = (Activity) context;
            mComunicaFragments = (IComunicaFragments) activity;
        }

    }

    @Override
    public void onEventoClick(int position) {

    }

    @Override
    public void onEventoDenunciadoClick(int position) {
        Evento eventoSeleccionado = adaptadorEvento.getItemAtIndexPosition(position);
        mComunicaFragments.MostrarDetalleEventoDenunciado(eventoSeleccionado);
    }


}
