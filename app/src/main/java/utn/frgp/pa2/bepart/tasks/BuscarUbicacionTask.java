package utn.frgp.pa2.bepart.tasks;

import android.os.AsyncTask;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import utn.frgp.pa2.bepart.entity.Categoria;
import utn.frgp.pa2.bepart.entity.EstadoEvento;
import utn.frgp.pa2.bepart.entity.Evento;
import utn.frgp.pa2.bepart.entity.Pais;
import utn.frgp.pa2.bepart.entity.Provincia;
import utn.frgp.pa2.bepart.entity.Ubicacion;
import utn.frgp.pa2.bepart.entity.Zona;
import utn.frgp.pa2.bepart.util.DBData;

public class BuscarUbicacionTask extends AsyncTask<Void, String, ArrayList<Ubicacion>> {
    private static final String QUERY = "{call sp_BuscarUbicacion(?,?)}";
    private int ubicacionInt;
    private String ubicacion;

    public BuscarUbicacionTask(int ubicacionInt, String ubicacion) {
        this.ubicacionInt = ubicacionInt;
        this.ubicacion = ubicacion;
    }

    @Override
    public ArrayList<Ubicacion> doInBackground(Void... voids) {
        Ubicacion ubicacion_temp = null;
        ArrayList<Ubicacion> resultado = new ArrayList<>();
        Connection connection = new DBData().getConnection();

        try {
            CallableStatement callableStatement = connection.prepareCall(QUERY);

            int i = 1;

            if(this.ubicacionInt == 0){
                callableStatement.setInt(i++, 0);
            }else{
                callableStatement.setInt(i++, this.ubicacionInt);
            }

            if(this.ubicacion.equals("")){
                callableStatement.setInt(i++, 0);
            }else{
                callableStatement.setString(i++, this.ubicacion);
            }


            callableStatement.execute();
            ResultSet resultSet = callableStatement.getResultSet();
            while(resultSet.next()){
                ubicacion_temp = new Ubicacion();
                ubicacion_temp.setIdUbicacion(resultSet.getInt("idUbicacion"));
                ubicacion_temp.setZona(new Zona(resultSet.getInt("idZona"),
                                new Provincia(resultSet.getInt("idProvincia"),
                                        new Pais(resultSet.getInt("idPais"),resultSet.getString("Pais")),
                                        resultSet.getString("Provincia")),
                                resultSet.getString("DescripcionZona"), resultSet.getString("CodigoPostal")));
                ubicacion_temp.setCalle(resultSet.getString("Calle"));
                ubicacion_temp.setLatitud(resultSet.getDouble("Latitud"));
                ubicacion_temp.setLongitud(resultSet.getDouble("Longitud"));

                resultado.add(ubicacion_temp);
            }
            connection.close();
        }catch(Exception ex){
            ex.printStackTrace();
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return resultado;
    }
}
