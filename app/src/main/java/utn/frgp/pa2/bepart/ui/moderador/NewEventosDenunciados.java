package utn.frgp.pa2.bepart.ui.moderador;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import utn.frgp.pa2.bepart.R;
import utn.frgp.pa2.bepart.entity.Evento;
import utn.frgp.pa2.bepart.tasks.BuscarEventosDenunciadosTask;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link NewEventosDenunciados#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NewEventosDenunciados extends Fragment implements NewDetalleEventoDenunciado.IEventoClickListener{

    private ListView listView;
    private ArrayAdapter<String> monthAdapter;
    private ArrayList<Evento> resultado;

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public NewEventosDenunciados() {
        // Required empty public constructor
    }

    public static NewEventosDenunciados newInstance(String param1, String param2) {
        NewEventosDenunciados fragment = new NewEventosDenunciados();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (container != null) {
            container.removeAllViews();
        }

        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_new_eventos_denunciados, container, false);
        listView = root.findViewById(R.id.lstEventosDenunciados);

        List<Evento> evento = new ArrayList<>();
        resultado = null;
        BuscarEventosDenunciadosTask buscarEventosTask = new BuscarEventosDenunciadosTask();
        buscarEventosTask.execute();
        try {
            resultado = buscarEventosTask.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        for (Evento _evento : resultado) {
            evento.add(_evento);
        }


        String[] pendientes = new String[evento.size()];
        for(int i = 0; i < evento.size(); i++){
                String Lugar = evento.get(i).getUbicacion().getCalle() + " - " + evento.get(i).getUbicacion().getZona().getDescripcionZona();
                if(evento.get(i).getUbicacion().getZona().getCodigoPostal() != null){
                    Lugar = Lugar + "(" + evento.get(i).getUbicacion().getZona().getCodigoPostal() + ")";
                }
                Lugar = Lugar + "\n" + evento.get(i).getUbicacion().getZona().getProvicia().getDescripcion() + " ," + evento.get(i).getUbicacion().getZona().getProvicia().getPais().getDescripcion();

                pendientes[i] =
                    "ID " + evento.get(i).getIdEvento() + " - " +
                    evento.get(i).getTitulo() + "     Evento " + evento.get(i).getCategoria().getDescripcion() + "\n"  +
                    "Organizador @" + evento.get(i).getUsuarioOrg() + "\n" +
                    Lugar + "\n";
        }

        monthAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, pendientes);
        listView.setAdapter(monthAdapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Evento eventoSeleccionado = evento.get(position);
                //Toast.makeText(getContext(),"Click en id "+ eventoSeleccionado.getIdEvento(),Toast.LENGTH_SHORT).show();
                NewDetalleEventoDenunciado newDetalleEvento = new NewDetalleEventoDenunciado(eventoSeleccionado, NewEventosDenunciados.this::onDarBajaEvento, position, monthAdapter);
                newDetalleEvento.show(getActivity().getSupportFragmentManager(), "NewDetalleEventoDenunciado");
            }
        });

        return root;
    }

    @Override
    public void onDarBajaEvento(int position) {
        ArrayList<Evento> evento = new ArrayList<>();
        for (Evento _evento : resultado) {
            if(_evento.getIdEvento() != position)
            evento.add(_evento);
        }

//        evento.remove(position);

        resultado = evento;

        String[] pendientes = new String[evento.size()];
        for(int i = 0; i < evento.size(); i++){
            String Lugar = evento.get(i).getUbicacion().getCalle() + " - " + evento.get(i).getUbicacion().getZona().getDescripcionZona();
            if(evento.get(i).getUbicacion().getZona().getCodigoPostal() != null){
                Lugar = Lugar + "(" + evento.get(i).getUbicacion().getZona().getCodigoPostal() + ")";
            }
            Lugar = Lugar + "\n" + evento.get(i).getUbicacion().getZona().getProvicia().getDescripcion() + " ," + evento.get(i).getUbicacion().getZona().getProvicia().getPais().getDescripcion();

            pendientes[i] =
                    "ID " + evento.get(i).getIdEvento() + " - " +
                            evento.get(i).getTitulo() + "     Evento " + evento.get(i).getCategoria().getDescripcion() + "\n"  +
                            "Organizador @" + evento.get(i).getUsuarioOrg() + "\n" +
                            Lugar + "\n";
        }

        ArrayAdapter<String> _monthAdapter2 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, pendientes);

        listView.setAdapter(_monthAdapter2);
    }
}