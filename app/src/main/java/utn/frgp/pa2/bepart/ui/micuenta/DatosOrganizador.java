package utn.frgp.pa2.bepart.ui.micuenta;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import utn.frgp.pa2.bepart.MainActivity;
import utn.frgp.pa2.bepart.Session.DocumentacionSession;
import utn.frgp.pa2.bepart.Session.UsuariosSession;
import utn.frgp.pa2.bepart.entity.Documentacion;
import utn.frgp.pa2.bepart.entity.EstadoDocumentacion;
import utn.frgp.pa2.bepart.entity.Usuario;
import utn.frgp.pa2.bepart.model.DocumentacionModel;
import utn.frgp.pa2.bepart.model.EstadoDocumentacionModel;
import utn.frgp.pa2.bepart.ui.moderador.RevisionSolicitudOrganizador;
import utn.frgp.pa2.bepart.util.ImagesServiceData;
import utn.frgp.pa2.bepart.R;
import utn.frgp.pa2.bepart.util.UploadImages;

public class DatosOrganizador extends Fragment implements View.OnClickListener{

    private static final int RESULT_LOAD_IMAGE = 1;
    private static final int RESULT_OK = -1; //no vino definida esta constante....
    private static final String SERVICE_URL = ImagesServiceData.save_url;
    private ImageView currentImage;

    private ImageView ivFrente, ivReverso, ivSelfie;

    //TextView tipo_documento;
    TextView nro_documento;
    Spinner spinner;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                              ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        if (container != null) {
            container.removeAllViews();
        }

        ImageView frente, reverso, selfie;
        Button submit;

        View view = inflater.inflate(R.layout.fragment_datos_organizador, container, false);
        submit = (Button) view.findViewById(R.id.submit);
        frente = (ImageView) view.findViewById(R.id.frente);
        reverso = (ImageView) view.findViewById(R.id.reverso);
        selfie = (ImageView) view.findViewById(R.id.selfie);
        //tipo_documento = (TextView) view.findViewById(R.id.tipo_documento);
        nro_documento = (TextView) view.findViewById(R.id.nro_documento);
        frente.setOnClickListener(this);
        reverso.setOnClickListener(this);
        selfie.setOnClickListener(this);
        submit.setOnClickListener(this);

        List<String> spinnerList = new ArrayList<String>();
        spinnerList.add("DNI");
        spinnerList.add("CI");
        spinnerList.add("LE");
        spinnerList.add("LC");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, spinnerList);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner = (Spinner) view.findViewById(R.id.spinner);
        spinner.setAdapter(adapter);

        TextView tvHistSol = view.findViewById(R.id.historial_solicitudes);
        tvHistSol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MisSolicitudesOrganizador misSolOrg = new MisSolicitudesOrganizador();
                misSolOrg.show(getActivity().getSupportFragmentManager(), "MisSolicitudesOrganizador");
            }
        });

        return view;
    }

    @Override
    public void onClick(View v){
        switch (v.getId()){
            case R.id.frente:
                currentImage = (ImageView) v.findViewById(R.id.frente);
                ivFrente = currentImage;
                break;
            case R.id.reverso:
                currentImage = (ImageView) v.findViewById(R.id.reverso);
                ivReverso = currentImage;
                break;
            case R.id.selfie:
                currentImage = (ImageView) v.findViewById(R.id.selfie);
                ivSelfie = currentImage;
                break;
            case R.id.submit:
                currentImage = null;
                break;
        }
        if(currentImage != null){
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, RESULT_LOAD_IMAGE);
        }
        else{

            try {
                onClickSubmit(v);
            } catch (IOException | ExecutionException | InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && data != null){
            Uri selectedImage = data.getData();
            currentImage.setImageURI(selectedImage);
        }
    }

    public void onClickSubmit(View v) throws IOException, ExecutionException, InterruptedException {

        if(ivFrente == null || ivReverso == null || ivSelfie == null ||
                //tipo_documento.getText().toString().length() == 0 ||
                nro_documento.getText().toString().length() == 0){
            Toast.makeText(getActivity(), "Complete todos los campos obligatorios", Toast.LENGTH_SHORT).show();
        }
        else {
            String imageName = "";

            UsuariosSession usuariosSession = new UsuariosSession(getActivity());
            Usuario usuario = usuariosSession.getUserFromSessionData();

            EstadoDocumentacion estadoDocumentacion = new EstadoDocumentacion();
            EstadoDocumentacionModel estadoDocumentacionModel = new EstadoDocumentacionModel();
            estadoDocumentacion.setIdEstadoDoc(EstadoDocumentacion.PENDIENTE_APROBACION);
            estadoDocumentacion = estadoDocumentacionModel.select(estadoDocumentacion).get(0);

            Documentacion documentacion = new Documentacion();
            DocumentacionModel documentacionModel = new DocumentacionModel();
            documentacion.setIdUsuario(usuario.getIdUsuario());
            documentacion.setEstadoDoc(estadoDocumentacion);
            documentacion.setIdDocumentacion(-1);

            if(documentacionModel.select(documentacion).size() == 0){
                documentacion.setNroDoc(nro_documento.getText().toString());
                documentacion.setTelefono("null");
                documentacion.setComentario("");
                documentacion.setTipo_documento(spinner.getSelectedItem().toString());

                BitmapDrawable bmd = (BitmapDrawable) ivFrente.getDrawable();
                Bitmap imageToUpload = bmd.getBitmap();
                imageName = this.nro_documento.getText().toString() + "-" + String.valueOf(ivFrente.getId()) + ".jpeg";
                new UploadImages(imageToUpload, imageName, getActivity()).execute();

                documentacion.setImgDNIFrente(imageName);

                bmd = (BitmapDrawable) ivReverso.getDrawable();
                imageToUpload = bmd.getBitmap();
                imageName = this.nro_documento.getText().toString() + "-" + String.valueOf(ivReverso.getId()) + ".jpeg";
                new UploadImages(imageToUpload, imageName, getActivity()).execute();

                documentacion.setImgDNIDorso(imageName);

                bmd = (BitmapDrawable) ivSelfie.getDrawable();
                imageToUpload = bmd.getBitmap();
                imageName = this.nro_documento.getText().toString() + "-" + String.valueOf(ivSelfie.getId()) + ".jpeg";
                new UploadImages(imageToUpload, imageName, getActivity()).execute();

                documentacion.setImgSelfie(imageName);

                if(documentacionModel.insert(documentacion)){
                    Toast.makeText(getActivity(), "Documentacion guardada correctamente", Toast.LENGTH_LONG).show();
                }
                else{
                    Toast.makeText(getActivity(), "Error al registrar", Toast.LENGTH_LONG).show();
                }

                Intent i = new Intent(getActivity(), MainActivity.class);
                startActivity(i);
            }
            else{
                Toast.makeText(getActivity(), "Error. Existe una solicitud pendiente.", Toast.LENGTH_LONG).show();
            }

        }
    }
}
