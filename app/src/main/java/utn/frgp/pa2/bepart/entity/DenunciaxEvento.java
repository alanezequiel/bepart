package utn.frgp.pa2.bepart.entity;

public class DenunciaxEvento {
    private int idDenuncia;
    private int idEvento;
    private String denunciante;
    private int Cantidad;
    private String TipoDenuncia;
    private String Comentario;
    private int CuposReales;

    public DenunciaxEvento() {
    }

    public DenunciaxEvento(int idDenuncia, int idEvento, String denunciante, int cantidad, String tipoDenuncia, String comentario) {
        this.idDenuncia = idDenuncia;
        this.idEvento = idEvento;
        this.denunciante = denunciante;
        this.Cantidad = cantidad;
        this.TipoDenuncia = tipoDenuncia;
        this.Comentario = comentario;
    }

    public int getIdDenuncia() {
        return idDenuncia;
    }

    public void setIdDenuncia(int idDenuncia) {
        this.idDenuncia = idDenuncia;
    }

    public int getIdEvento() {
        return idEvento;
    }

    public void setIdEvento(int idEvento) {
        this.idEvento = idEvento;
    }

    public String getDenunciante() {
        return denunciante;
    }

    public void setDenunciante(String denunciante) {
        this.denunciante = denunciante;
    }

    public String getTipoDenuncia() {
        return TipoDenuncia;
    }

    public void setTipoDenuncia(String tipoDenuncia) {
        this.TipoDenuncia = tipoDenuncia;
    }

    public String getComentario() {
        return Comentario;
    }

    public void setComentario(String comentario) {
       this.Comentario = comentario;
    }

    public int getCantidad() {
        return Cantidad;
    }

    public void setCantidad(int cantidad) {
        Cantidad = cantidad;
    }

    public int getCuposReales() {
        return CuposReales;
    }

    public void setCuposReales(int cuposReales) {
        CuposReales = cuposReales;
    }
}
