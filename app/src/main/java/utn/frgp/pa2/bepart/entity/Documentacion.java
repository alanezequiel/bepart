package utn.frgp.pa2.bepart.entity;

import java.io.Serializable;
import java.util.Date;

public class Documentacion implements Serializable {

    private int idDocumentacion;
    private int idUsuario;
    private String Username;
    private String nroDoc;
    private String telefono;
    private Date fechaSolicitud;
    private String imgDNIFrente;
    private String imgDNIDorso;
    private String imgSelfie;
    private EstadoDocumentacion estadoDoc;
    private String comentario;
    private String tipo_documento;

    public Documentacion(){

    }

    public Documentacion(int idUsuario, String nroDoc, String telefono, Date fechaSolicitud,
                         String imgDNIFrente, String imgDNIDorso, String imgSelfie,
                         EstadoDocumentacion estadoDoc, String comentario) {
        this.idUsuario = idUsuario;
        this.nroDoc = nroDoc;
        this.telefono = telefono;
        this.fechaSolicitud = fechaSolicitud;
        this.imgDNIFrente = imgDNIFrente;
        this.imgDNIDorso = imgDNIDorso;
        this.imgSelfie = imgSelfie;
        this.estadoDoc = estadoDoc;
        this.comentario = comentario;
    }

    public int getIdDocumentacion() {
        return idDocumentacion;
    }

    public void setIdDocumentacion(int idDocumentacion) {
        this.idDocumentacion = idDocumentacion;
    }

    public String getTipo_documento() {
        return tipo_documento;
    }

    public void setTipo_documento(String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNroDoc() {
        return nroDoc;
    }

    public void setNroDoc(String nroDoc) {
        this.nroDoc = nroDoc;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Date getFechaSolicitud() {
        return fechaSolicitud;
    }

    public void setFechaSolicitud(Date fechaSolicitud) {
        this.fechaSolicitud = fechaSolicitud;
    }

    public String getImgDNIFrente() {
        return imgDNIFrente;
    }

    public void setImgDNIFrente(String imgDNIFrente) {
        this.imgDNIFrente = imgDNIFrente;
    }

    public String getImgDNIDorso() {
        return imgDNIDorso;
    }

    public void setImgDNIDorso(String imgDNIDorso) {
        this.imgDNIDorso = imgDNIDorso;
    }

    public String getImgSelfie() {
        return imgSelfie;
    }

    public void setImgSelfie(String imgSelfie) {
        this.imgSelfie = imgSelfie;
    }

    public EstadoDocumentacion getEstadoDoc() {
        return estadoDoc;
    }

    public void setEstadoDoc(EstadoDocumentacion estadoDoc) {
        this.estadoDoc = estadoDoc;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }
}
