package utn.frgp.pa2.bepart.tasks;

import android.os.AsyncTask;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import utn.frgp.pa2.bepart.entity.AsistenciaxEvento;
import utn.frgp.pa2.bepart.entity.Categoria;
import utn.frgp.pa2.bepart.entity.EstadoEvento;
import utn.frgp.pa2.bepart.entity.Evento;
import utn.frgp.pa2.bepart.entity.Pais;
import utn.frgp.pa2.bepart.entity.Provincia;
import utn.frgp.pa2.bepart.entity.Ubicacion;
import utn.frgp.pa2.bepart.entity.Usuario;
import utn.frgp.pa2.bepart.entity.Zona;
import utn.frgp.pa2.bepart.util.DBData;

public class BuscarAsistenciaEventoTask extends AsyncTask<Void, String, ArrayList<AsistenciaxEvento>> {

    private static final String QUERY = "{call sp_BuscarAsistenciaEventos(?,?)}";
    private Usuario usuario;
    private int idEvento;

    public BuscarAsistenciaEventoTask(Usuario idUsuario, int idEvento) {
        this.usuario = idUsuario;
        this.idEvento = idEvento;
    }

    @Override
    protected ArrayList<AsistenciaxEvento> doInBackground(Void... voids) {
        AsistenciaxEvento axevento_temp = null;
        Evento evento_temp = null;
        ArrayList<AsistenciaxEvento> resultado = new ArrayList<>();
        Connection connection = new DBData().getConnection();

        try {
            CallableStatement callableStatement = connection.prepareCall(QUERY);
            int i = 1;

            if(this.usuario == null){
                callableStatement.setInt(i++, 0);
            }else{
                callableStatement.setInt(i++, this.usuario.getIdUsuario());
            }

            if(this.idEvento == 0){
                callableStatement.setInt(i++, 0);
            }else{
                callableStatement.setInt(i++, this.idEvento);
            }

            callableStatement.execute();
            ResultSet resultSet = callableStatement.getResultSet();
            while(resultSet.next()){
                axevento_temp = new AsistenciaxEvento();
                axevento_temp.setIdAsistencia(resultSet.getInt("IdAsistencia"));
                axevento_temp.setUsuario(usuario);
                axevento_temp.setEvento(new Evento());
                evento_temp = new Evento();
                    evento_temp.setIdEvento(resultSet.getInt("idEvento"));
                    evento_temp.setTitulo(resultSet.getString("Titulo"));
                    evento_temp.setDescripcion(resultSet.getString("Descripcion"));
                    evento_temp.setUbicacion(new Ubicacion(resultSet.getInt("idUbicacion"),
                            new Zona(resultSet.getInt("idZona"),
                                    new Provincia(resultSet.getInt("idProvincia"),
                                            new Pais(resultSet.getInt("idPais"),resultSet.getString("Pais")),
                                            resultSet.getString("Provincia")),
                                    resultSet.getString("DescripcionZona"), resultSet.getString("CodigoPostal")),
                            resultSet.getString("Calle"),resultSet.getDouble("Latitud"), resultSet.getDouble("Longitud")));
                    evento_temp.setFecha(resultSet.getString("Fecha"));
                    evento_temp.setHora(resultSet.getString("Hora"));
                    evento_temp.setCupos(resultSet.getInt("Cupos"));
                    evento_temp.setImagen(resultSet.getString("Imagen"));
                    evento_temp.setCategoria(new Categoria(resultSet.getInt("idCategoria"), resultSet.getString("Categoria")));
                    evento_temp.setEstadoEvento(new EstadoEvento(resultSet.getInt("idEstadoEvento"),resultSet.getString("EstadoEvento")));
                    evento_temp.setUsuarioOrg(resultSet.getString("UsuarioOrg"));
                axevento_temp.setEvento(evento_temp);

                resultado.add(axevento_temp);
            }
            connection.close();
        }catch(Exception ex){
            ex.printStackTrace();
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return  resultado;
    }
}
