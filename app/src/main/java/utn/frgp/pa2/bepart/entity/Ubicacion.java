package utn.frgp.pa2.bepart.entity;

public class Ubicacion {
    private int idUbicacion;
    private Zona zona;
    private String calle;
    private double latitud;
    private double longitud;

    public Ubicacion() {
    }

    public Ubicacion(int idUbicacion, Zona zona, String calle, double latitud, double longitud) {
        this.idUbicacion = idUbicacion;
        this.zona = zona;
        this.calle = calle;
        this.latitud = latitud;
        this.longitud = longitud;
    }

    public int getIdUbicacion() {
        return idUbicacion;
    }

    public Zona getZona() {
        return zona;
    }

    public String getCalle() {
        return calle;
    }

    public double getLatitud() {
        return latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setIdUbicacion(int idUbicacion) {
        this.idUbicacion = idUbicacion;
    }

    public void setZona(Zona zona) {
        this.zona = zona;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }
}
