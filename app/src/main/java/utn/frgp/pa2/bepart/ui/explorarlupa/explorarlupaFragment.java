package utn.frgp.pa2.bepart.ui.explorarlupa;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import utn.frgp.pa2.bepart.R;
import utn.frgp.pa2.bepart.entity.Categoria;
import utn.frgp.pa2.bepart.entity.Comentario;
import utn.frgp.pa2.bepart.entity.Evento;
import utn.frgp.pa2.bepart.entity.Provincia;
import utn.frgp.pa2.bepart.entity.Zona;
import utn.frgp.pa2.bepart.interfaces.IComunicaFragments;
import utn.frgp.pa2.bepart.recyclerview.RecyclerViewAdaptadorEvento;
import utn.frgp.pa2.bepart.tasks.BuscarCategoriaTask;
import utn.frgp.pa2.bepart.tasks.BuscarComentariosTask;
import utn.frgp.pa2.bepart.tasks.BuscarEventosTask;
import utn.frgp.pa2.bepart.tasks.BuscarZonaTask;

public class explorarlupaFragment extends Fragment implements RecyclerViewAdaptadorEvento.IEventoClickListener  {

    private static final String TAG = "explorarlupafragment";

    private RecyclerView recyclerViewEvento;
    private RecyclerViewAdaptadorEvento adaptadorEvento;

    private Activity activity;
    private IComunicaFragments mComunicaFragments;

    private int cantidadCalificaciones = 0;
    private int calificacionTotalEvento = 0;

    //private SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yyyy");
    //private SimpleDateFormat dh = new SimpleDateFormat("HH:mm aa");

    private Spinner spnZona;
    private Spinner spnCategoria;
    private Button imgBuscar;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public explorarlupaFragment() {
        // Required empty public constructor
    }

    public static explorarlupaFragment newInstance(String param1, String param2) {
        explorarlupaFragment fragment = new explorarlupaFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (container != null) {
            container.removeAllViews();
        }
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_explorar_lupa, container, false);
        recyclerViewEvento = root.findViewById(R.id.rvItemEventos);
        recyclerViewEvento.setLayoutManager(new LinearLayoutManager(getContext()));
        spnZona = (Spinner) root.findViewById(R.id.spnZona);
        spnCategoria = (Spinner) root.findViewById(R.id.spnCategoria);
        imgBuscar = (Button) root.findViewById(R.id.imgBuscar);



        ArrayList<Zona> listaZona = new ArrayList<Zona>();
        try {
            listaZona = obtenerZonas();
            ArrayAdapter<Zona> adpZona = new ArrayAdapter<Zona>(getContext(), android.R.layout.simple_spinner_item, listaZona);
            adpZona.setDropDownViewResource(android.R.layout.simple_list_item_checked);
            spnZona.setAdapter(adpZona);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ArrayList<Categoria> listaCategoria = new ArrayList<Categoria>();
        try {
            listaCategoria = obtenerCategorias();
            ArrayAdapter<Categoria> adpCategoria = new ArrayAdapter<Categoria>(getContext(), android.R.layout.simple_spinner_item,listaCategoria);
            adpCategoria.setDropDownViewResource(android.R.layout.simple_list_item_checked);
            spnCategoria.setAdapter(adpCategoria);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        imgBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Categoria cs= (Categoria) spnCategoria.getSelectedItem();
                Zona zs = (Zona) spnZona.getSelectedItem();
                Toast.makeText(getContext(), "Zona : " + zs.getDescripcionZona() + " Categoria : " + cs.getDescripcion(), Toast.LENGTH_SHORT).show();
                try {
                    cargarEventos(cs.getIdCategoria(),zs.getIdZona());
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        /*
        try {
            adaptadorEvento = new RecyclerViewAdaptadorEvento(obtenerEventos(), this);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        recyclerViewEvento.setAdapter(adaptadorEvento);*/
        try {
            cargarEventos(0,0); //(0,0) = TODOS LOS EVENTOS
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return root;
    }

    public void cargarEventos(int idCategoria,int idZona) throws ExecutionException, InterruptedException {
        adaptadorEvento = new RecyclerViewAdaptadorEvento(obtenerEventos(idCategoria,idZona), this);
        recyclerViewEvento.setAdapter(adaptadorEvento);
    }

    //OBTENGO LOS EVENTOS (0,0) TODOS
    public List<Evento> obtenerEventos(int idCategoria,int idZona) throws ExecutionException, InterruptedException {
        List <Evento> evento = new ArrayList<>();
        ArrayList<Evento> resultado = null;
        BuscarEventosTask buscarEventosTask = new BuscarEventosTask(idCategoria,idZona,getContext());
        buscarEventosTask.execute();
        resultado = buscarEventosTask.get();


        for (Evento _evento : resultado) {

           /* ArrayList<Comentario> comentarios = obtenerComentarios(_evento.getIdEvento());
            cantidadCalificaciones = comentarios.size();
            for(Comentario comentario : comentarios){
                calificacionTotalEvento += comentario.getPuntaje();
            }
            float calif = cantidadCalificaciones != 0 ? calificacionTotalEvento / cantidadCalificaciones : 0;
            _evento.setCalificacion(calif);*/

            evento.add(_evento);
        }

        return evento;
    }

    public ArrayList<Categoria> obtenerCategorias() throws ExecutionException, InterruptedException {
        ArrayList<Categoria> categoria = new ArrayList<>();
        ArrayList<Categoria> resultado = null;
        BuscarCategoriaTask buscarCategoriaTask = new BuscarCategoriaTask(0);
        buscarCategoriaTask.execute();
        resultado = buscarCategoriaTask.get();

        categoria.add(new Categoria(0,"Seleccionar"));

        for (Categoria _categoria : resultado) {
            categoria.add(_categoria);
        }

        return categoria;
    }

    public ArrayList<Zona> obtenerZonas() throws ExecutionException, InterruptedException {
        ArrayList<Zona> zona = new ArrayList<>();
        ArrayList<Zona> resultado = null;
        BuscarZonaTask buscarZonaTask = new BuscarZonaTask(0,"0");
        buscarZonaTask.execute();
        resultado = buscarZonaTask.get();

        zona.add(new Zona(0,new Provincia(),"Seleccionar","0"));

        for (Zona _zona : resultado) {
            zona.add(_zona);
        }

        return zona;
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if(context instanceof Activity){
            activity = (Activity) context;
            mComunicaFragments = (IComunicaFragments) activity;
        }

    }


    @Override
    public void onEventoClick(int position) {
        Evento eventoSeleccionado = adaptadorEvento.getItemAtIndexPosition(position);

        try {
            ArrayList<Comentario> comentarios = obtenerComentarios(eventoSeleccionado.getIdEvento());
            mComunicaFragments.MostrarDetalleEvento(eventoSeleccionado, comentarios);

        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onEventoDenunciadoClick(int position) {

    }

    public ArrayList<Comentario> obtenerComentarios(int idEvento) throws ExecutionException, InterruptedException {
        ArrayList<Comentario> comentarios = new ArrayList<>();

        BuscarComentariosTask comentariosTask = new BuscarComentariosTask(idEvento);
        comentariosTask.execute();
        comentarios = comentariosTask.get();

        return comentarios;
    }


}