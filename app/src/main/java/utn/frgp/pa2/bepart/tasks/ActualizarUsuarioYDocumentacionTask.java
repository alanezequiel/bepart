package utn.frgp.pa2.bepart.tasks;

import android.os.AsyncTask;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import utn.frgp.pa2.bepart.entity.Usuario;
import utn.frgp.pa2.bepart.util.DBData;

public class ActualizarUsuarioYDocumentacionTask extends AsyncTask<Void, String, Integer> {

    private static final String QUERY ="{call sp_ActualizarUsuarioYDocumenacion(?,?,?,?,?)}";
    private int idDocumentacion;
    private int idUsuario;
    private int idEstadoDocumentacion;
    private int idEstadoUsuario;
    private String comentario;

    public ActualizarUsuarioYDocumentacionTask(int idDoc, int idUsu, int idEstDoc, int idEstUsu, String comentario){
        this.idDocumentacion = idDoc;
        this.idUsuario = idUsu;
        this.idEstadoDocumentacion = idEstDoc;
        this.idEstadoUsuario = idEstUsu;
        this.comentario = comentario;
    }
    @Override
    public Integer doInBackground(Void... voids) {

        Integer temp = new Integer(0);
        Connection connection = new DBData().getConnection();
        try{

            CallableStatement preparedStatement = connection.prepareCall(QUERY);

            int i = 1;
            preparedStatement.setInt(i++, this.idDocumentacion);
            preparedStatement.setInt(i++, this.idUsuario);
            preparedStatement.setInt(i++, this.idEstadoDocumentacion);
            preparedStatement.setInt(i++, this.idEstadoUsuario);
            preparedStatement.setString(i++, this.comentario);

            temp = preparedStatement.executeUpdate();
            connection.close();
        }catch(Exception ex){
            ex.printStackTrace();
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return temp;
    }
}

