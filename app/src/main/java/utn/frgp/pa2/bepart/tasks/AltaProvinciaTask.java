package utn.frgp.pa2.bepart.tasks;

import android.os.AsyncTask;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import utn.frgp.pa2.bepart.entity.Provincia;
import utn.frgp.pa2.bepart.util.DBData;

public class AltaProvinciaTask extends AsyncTask<Void, String, Integer> {

    private static final String QUERY ="{call sp_RegistrarProvincia(?,?,?)}";
    private Provincia provincia;
    public AltaProvinciaTask(Provincia provincia){ this.provincia = provincia; }
    @Override
    public Integer doInBackground(Void... voids) {
        Integer temp = new Integer(0);
        Connection connection = new DBData().getConnection();
        try{

            CallableStatement preparedStatement = connection.prepareCall(QUERY);

            int i = 1;
            preparedStatement.setInt(i++, this.provincia.getPais().getIdPais());
            preparedStatement.setString(i++, this.provincia.getDescripcion());
            preparedStatement.setInt(i++, 1); //El estado

            temp = preparedStatement.executeUpdate();
            connection.close();
        }catch(Exception ex){
            ex.printStackTrace();
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return temp;
    }
}
