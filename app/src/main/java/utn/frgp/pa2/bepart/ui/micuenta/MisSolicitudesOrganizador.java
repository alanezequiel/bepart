package utn.frgp.pa2.bepart.ui.micuenta;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import utn.frgp.pa2.bepart.R;
import utn.frgp.pa2.bepart.Session.DocumentacionSession;
import utn.frgp.pa2.bepart.Session.UsuariosSession;
import utn.frgp.pa2.bepart.entity.Documentacion;
import utn.frgp.pa2.bepart.entity.EstadoDocumentacion;
import utn.frgp.pa2.bepart.entity.Usuario;
import utn.frgp.pa2.bepart.model.DocumentacionModel;
import utn.frgp.pa2.bepart.ui.moderador.RevisionSolicitudOrganizador;


public class MisSolicitudesOrganizador extends DialogFragment {

    ListView listView;
    ArrayList<Documentacion> docPendientes = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        if (container != null) {
            container.removeAllViews();
        }

        View view = inflater.inflate(R.layout.fragment_mis_solicitudes_organizador, container, false);

        listView = view.findViewById(R.id.list_view);

        UsuariosSession usuSession = new UsuariosSession(getContext());
        Usuario usu = usuSession.getUserFromSessionData();

        DocumentacionModel documentacionModel = new DocumentacionModel();
        Documentacion documentacion = new Documentacion();
        documentacion.setIdDocumentacion(-1);
        EstadoDocumentacion estadoDocumentacion = new EstadoDocumentacion();
        estadoDocumentacion.setIdEstadoDoc(-1);
        documentacion.setEstadoDoc(estadoDocumentacion);
        documentacion.setIdUsuario(usu.getIdUsuario());
        try {
            docPendientes = documentacionModel.select(documentacion);
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

        String[] pendientes = new String[docPendientes.size()];
        for(int i = 0; i < docPendientes.size(); i++){
            String comentario = docPendientes.get(i).getComentario();
            if( comentario.length() > 50){
                comentario = comentario.substring(0, 49);
            }
            pendientes[i] =
                    "Estado: " + docPendientes.get(i).getEstadoDoc().getDescripcion().toUpperCase() + "\n" +
                    "Fecha: " + docPendientes.get(i).getFechaSolicitud() + "\n" +
                    "Comentario: " + docPendientes.get(i).getComentario();
        }
        if(docPendientes.size() == 0){
            pendientes = new String[1];
            pendientes[0] = "No hay solicitudes";
        }
        ArrayAdapter<String> monthAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, pendientes);
        listView.setAdapter(monthAdapter);

        return view;
    }

}