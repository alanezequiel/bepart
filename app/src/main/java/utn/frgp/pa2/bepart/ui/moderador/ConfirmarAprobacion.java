package utn.frgp.pa2.bepart.ui.moderador;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import utn.frgp.pa2.bepart.MainActivity;
import utn.frgp.pa2.bepart.R;
import utn.frgp.pa2.bepart.Session.DocumentacionSession;
import utn.frgp.pa2.bepart.Session.Session;
import utn.frgp.pa2.bepart.Session.SessionData;
import utn.frgp.pa2.bepart.entity.Documentacion;
import utn.frgp.pa2.bepart.entity.EstadoDocumentacion;
import utn.frgp.pa2.bepart.entity.TipoUsuario;
import utn.frgp.pa2.bepart.model.DocumentacionModel;
import utn.frgp.pa2.bepart.tasks.AltaDenunciaTask;

public class ConfirmarAprobacion extends DialogFragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_confirmar_aprobar_solicitud, container, false);

        Button confirmar = view.findViewById(R.id.confirmar);

        confirmar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msj;
                DocumentacionModel docModel = new DocumentacionModel();
                DocumentacionSession docSession = new DocumentacionSession(getContext());
                Documentacion documentacion = docSession.getDocumentacionFromSessionData();
                documentacion.setComentario("Aprobado");
                try {
                    docModel.update(
                            documentacion, EstadoDocumentacion.APROBADA, TipoUsuario.ORGANIZADOR
                    );
                    msj = "Actualizacion Correcta";
                } catch (ExecutionException | InterruptedException e) {
                    e.printStackTrace();
                    msj = "Error al actualizar";
                }
                Toast.makeText(getContext(), msj, Toast.LENGTH_LONG).show();
                //getActivity().getFragmentManager().popBackStack();
                //getDialog().dismiss();
                Intent i = new Intent(getActivity(), MainActivity.class);
                startActivity(i);
            }
        });
        return view;
    }
}
