package utn.frgp.pa2.bepart.tasks;

import android.os.AsyncTask;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;

import utn.frgp.pa2.bepart.entity.Categoria;
import utn.frgp.pa2.bepart.entity.EstadoUsuario;
import utn.frgp.pa2.bepart.entity.TipoUsuario;
import utn.frgp.pa2.bepart.entity.Usuario;
import utn.frgp.pa2.bepart.util.DBData;

public class BuscarCategoriaTask extends AsyncTask<Void, String, ArrayList<Categoria>> {
    private static final String QUERY = "{call sp_BuscarCategoria(?)}";
    private int IdCategoria;

    public BuscarCategoriaTask(int idCategoria){
        this.IdCategoria = idCategoria;
    }

    @Override
    public ArrayList<Categoria> doInBackground(Void... voids) {
        Categoria categoria_temp = null;
        ArrayList<Categoria> resultado = new ArrayList<>();
        Connection connection = new DBData().getConnection();

        try {

            CallableStatement callableStatement = connection.prepareCall(QUERY);

            int i = 1;

            if(this.IdCategoria == 0){
                callableStatement.setInt(i++, 0);
            }

            callableStatement.execute();
            ResultSet resultSet = callableStatement.getResultSet();
            while(resultSet.next()){
                categoria_temp = new Categoria();
                categoria_temp.setIdCategoria(resultSet.getInt("IdCategoria"));
                categoria_temp.setDescripcion(resultSet.getString("Descripcion"));

                resultado.add(categoria_temp);
            }
            connection.close();
        }catch(Exception ex){
            ex.printStackTrace();
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return resultado;
    }
}
