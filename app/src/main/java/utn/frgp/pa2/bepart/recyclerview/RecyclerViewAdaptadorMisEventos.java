package utn.frgp.pa2.bepart.recyclerview;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import utn.frgp.pa2.bepart.R;
import utn.frgp.pa2.bepart.dialogos.DialogoCalificarEvento;
import utn.frgp.pa2.bepart.entity.Evento;
import utn.frgp.pa2.bepart.entity.Usuario;
import utn.frgp.pa2.bepart.tasks.ActualizarEstadoEventoTask;

public class RecyclerViewAdaptadorMisEventos extends RecyclerView.Adapter<RecyclerViewAdaptadorMisEventos.ViewHolderData> {

    private static final String TAG = "RecyclerViewAdaptadorMi";
    public List<Evento> listEventos;
    private Usuario ssUser;
    private Context context;
    private Fragment fragment;
    //private SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yy");
    //private SimpleDateFormat dh = new SimpleDateFormat("HH:mm");

    private IEventoClickListener mEventoClick;

    public RecyclerViewAdaptadorMisEventos(List<Evento> listEventos, Usuario ssUser, Context context, IEventoClickListener eventoClicklistener, Fragment fragment) {
        this.listEventos = listEventos;
        this.ssUser = ssUser;
        this.context = context;
        this.mEventoClick = eventoClicklistener;
        this.fragment = fragment;
    }

    @NonNull
    @Override
    public ViewHolderData onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_miseventos,parent,false);
        ViewHolderData viewHolder = new ViewHolderData(view, mEventoClick);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderData holder, int position) {
        String Lugar = listEventos.get(position).getUbicacion().getCalle() + " - " + listEventos.get(position).getUbicacion().getZona().getDescripcionZona();
        if(listEventos.get(position).getUbicacion().getZona().getCodigoPostal() != null){
            Lugar = Lugar + "(" + listEventos.get(position).getUbicacion().getZona().getCodigoPostal() + ")";
        }
        Lugar = Lugar + "\n" + listEventos.get(position).getUbicacion().getZona().getProvicia().getDescripcion() + " ," + listEventos.get(position).getUbicacion().getZona().getProvicia().getPais().getDescripcion();

        String dateUsed = listEventos.get(position).getFecha();
        String[] parts = dateUsed.split("-");
        String year = parts[0];
        String month = parts[1];
        String day = parts[2];

        String nuevaFecha = day + "/" + month + "/" + year;

        String timeUsed = listEventos.get(position).getHora();
        String[] partTime = timeUsed.split(":");
        String hour = partTime[0];
        String minute = partTime[1];
        String second = partTime[2];

        String nuevaHora = hour + ":" + minute;

        holder.Fecha.setText(nuevaFecha + " " + nuevaHora);
        holder.Titulo.setText(listEventos.get(position).getTitulo());
        holder.Direccion.setText(Lugar);
        holder.Descripcion.setText("Evento " + listEventos.get(position).getEstadoEvento().getDescripcion());
        String user = ssUser.getUsername();
        if (user.equals(listEventos.get(position).getUsuarioOrg())){
            holder.llContainer.setBackgroundColor(Color.rgb(73, 123, 180));
            holder.Calificar.setVisibility(View.INVISIBLE);
            holder.Finalizar.setVisibility(View.VISIBLE);
            if(listEventos.get(position).getEstadoEvento().getIdEstadoEvento() == 2){
                holder.Finalizar.setVisibility(View.INVISIBLE);
            }
        }else{
            holder.llContainer.setBackgroundColor(Color.rgb(75, 167, 171 ));
            holder.Calificar.setVisibility(View.VISIBLE);
            holder.Descripcion.setText("Evento " + listEventos.get(position).getEstadoEvento().getDescripcion() + "\n Organizado por " + listEventos.get(position).getUsuarioOrg().toUpperCase());
            holder.Finalizar.setVisibility(View.INVISIBLE);
        }

        if(listEventos.get(position).getEstadoEvento().getIdEstadoEvento() == 0){
            holder.Calificar.setVisibility(View.INVISIBLE);
            holder.Finalizar.setVisibility(View.INVISIBLE);
        }

        holder.Calificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "onClick: !ASD" );
                new DialogoCalificarEvento(context, listEventos.get(holder.getAdapterPosition()), ssUser.getIdUsuario(), fragment);
            }
        });

        holder.Finalizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listEventos.get(position).getEstadoEvento().getIdEstadoEvento() == 2){
                    Toast.makeText(context,"Evento ya Finalizado",Toast.LENGTH_SHORT).show();
                }else{
                    ActualizarEstadoEventoTask actualizarEstadoEventoTask = new ActualizarEstadoEventoTask(listEventos.get(position).getIdEvento(),2);
                    actualizarEstadoEventoTask.execute();
                    holder.listener.onFinalzarEventoClick(position);
                    Toast.makeText(context,"Evento Finalizado Correctamente",Toast.LENGTH_SHORT).show();
                }
            }
        });



    }

    @Override
    public int getItemCount() {
        return listEventos.size();
    }


    public Evento getItemAtIndexPosition(int position){
        return listEventos.get(position);
    }

    public interface IEventoClickListener{
        void onEventoClick(int position);
        void onFinalzarEventoClick(int position);
    }

    public static class ViewHolderData extends RecyclerView.ViewHolder implements View.OnClickListener{

        private IEventoClickListener listener;
        private TextView Fecha,Titulo,Direccion,Descripcion,Calificar,Finalizar;
        private LinearLayout llContainer;

        public ViewHolderData(@NonNull View itemView, IEventoClickListener _listener) {
            super(itemView);
            Fecha = (TextView) itemView.findViewById(R.id.tvFechaEv);
            Titulo = (TextView) itemView.findViewById(R.id.tvTitle);
            Direccion = (TextView) itemView.findViewById(R.id.tvDirection);
            Descripcion = (TextView) itemView.findViewById(R.id.tvDescription);
            Calificar = (TextView) itemView.findViewById(R.id.tvDate);
            Finalizar = (TextView) itemView.findViewById(R.id.tvFinalizar);
            llContainer = (LinearLayout) itemView.findViewById(R.id.llContainer);
            this.listener = _listener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

            if(view.getId() == R.id.tvFinalizar){
                Log.e(TAG, "onClick: sdfojsdifj --- sdfsdfsdf ---sdfsdf");
                // listener.onFinalzarEventoClick(getAdapterPosition());
            }else {
                listener.onEventoClick(getAdapterPosition());
            }
        }
    }
}
