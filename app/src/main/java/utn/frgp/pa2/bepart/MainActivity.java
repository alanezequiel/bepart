package utn.frgp.pa2.bepart;

import android.os.Bundle;
import android.view.View;
import android.view.Menu;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.util.ArrayList;

import utn.frgp.pa2.bepart.Session.UsuariosSession;
import utn.frgp.pa2.bepart.entity.Comentario;
import utn.frgp.pa2.bepart.entity.Evento;
import utn.frgp.pa2.bepart.entity.Usuario;
import utn.frgp.pa2.bepart.interfaces.IComunicaFragments;
import utn.frgp.pa2.bepart.ui.detalleevento.DetalleEvento;
import utn.frgp.pa2.bepart.ui.moderador.DetalleEventoDenunciadoFragment;
import utn.frgp.pa2.bepart.ui.moderador.EventosDenunciadosFragment;
import utn.frgp.pa2.bepart.ui.organizador.eventos.NuevoEventoFragment;

public class MainActivity extends AppCompatActivity implements IComunicaFragments {

    private static final String TAG = "MainActivity";

    private AppBarConfiguration mAppBarConfiguration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navigation_menu);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.hide();

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSupportFragmentManager().beginTransaction().replace(
                        R.id.nav_host_fragment,new NuevoEventoFragment()).setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).commit();
            }
        });


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        //Arranco con los controles del nav
        View hView = navigationView.getHeaderView(0);
        TextView txtNombre = (TextView) hView.findViewById(R.id.txtNombre);
        TextView txtHola = (TextView) hView.findViewById(R.id.txtHola);
        ImageView imgIni = (ImageView) hView.findViewById(R.id.imageView);
        TextView txtTipoUsuario = (TextView) hView.findViewById(R.id.tvTipoUsuario);
        ImageView imgCheck = (ImageView) hView.findViewById(R.id.imgCheck);
        txtTipoUsuario.setVisibility(View.INVISIBLE);
        imgCheck.setVisibility(View.INVISIBLE);

        UsuariosSession usuariosSession = new UsuariosSession(getApplicationContext());
        Usuario ssUser =  usuariosSession.getUserFromSessionData();
        String strUsuarioTipo="nouser";

        if(ssUser != null){
            strUsuarioTipo = ssUser.getTipoUsuario().getDescripcion().toLowerCase();
            navigationView.getMenu().setGroupVisible(R.id.group_generic, true);
        }
        //Aca hay que validar el Session //hacer un case

        switch (strUsuarioTipo){
            case "usuario":
                txtHola.setVisibility(View.INVISIBLE);
                txtNombre.setText(ssUser.getNombre() + " " + ssUser.getApellido());
                imgIni.setImageResource(R.mipmap.profile);
                navigationView.getMenu().setGroupVisible(R.id.group_usuario, true); //Con esto mostramos los grupos del Menu Usuario
                break;

            case "organizador":
                    fab.show();
                    txtHola.setVisibility(View.INVISIBLE);
                    txtTipoUsuario.setText(strUsuarioTipo);
                    txtTipoUsuario.setVisibility(View.VISIBLE);
                    txtNombre.setText(ssUser.getNombre() + " " + ssUser.getApellido());
                    imgIni.setImageResource(R.mipmap.profile);
                    navigationView.getMenu().setGroupVisible(R.id.group_organizador, true); //Con esto mostramos los grupos del Menu Organizador
                    imgCheck.setVisibility(View.VISIBLE);
                break;

            case "moderador":
                txtHola.setVisibility(View.INVISIBLE);
                txtTipoUsuario.setText(strUsuarioTipo);
                txtTipoUsuario.setVisibility(View.VISIBLE);
                txtNombre.setText(ssUser.getNombre() + " " + ssUser.getApellido());
                imgIni.setImageResource(R.mipmap.profile);
                navigationView.getMenu().setGroupVisible(R.id.group_moderador, true); //Con esto mostramos los grupos del Menu Moderador
                imgCheck.setVisibility(View.VISIBLE);
                break;

            case "nouser":
                    navigationView.getMenu().setGroupVisible(R.id.group_nouser, true); //Con esto mostramos los grupos del Menu nouser
                break;
        }

        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home)//, R.id.nav_gallery, R.id.nav_slideshow) Aca hago que el nav_home sea como principal, si entramos a otro nos tira la flechita de volver atras.
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }


    @Override
    public void MostrarDetalleEvento(Evento eventoSeleccionado, ArrayList<Comentario> comentarios) {
        getSupportFragmentManager().beginTransaction().replace(R.id.container_ini,
                DetalleEvento.newInstance(eventoSeleccionado, comentarios)).setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).commit();
    }

    @Override
    public void MostrarDatosEdicionEvento(Evento eventoSeleccionado) {
        getSupportFragmentManager().beginTransaction().replace(R.id.container_ini,
                NuevoEventoFragment.newInstance(eventoSeleccionado)).setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).commit();
    }

    @Override
    public void MostrarDetalleEventoDenunciado(Evento eventoSeleccionado) {
        getSupportFragmentManager().beginTransaction().replace(R.id.container_ini,
                DetalleEventoDenunciadoFragment.newInstance(eventoSeleccionado)).setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).commit();
    }
}
