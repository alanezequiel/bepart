package utn.frgp.pa2.bepart.tasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import utn.frgp.pa2.bepart.entity.Categoria;
import utn.frgp.pa2.bepart.entity.EstadoEvento;
import utn.frgp.pa2.bepart.entity.Evento;
import utn.frgp.pa2.bepart.entity.Pais;
import utn.frgp.pa2.bepart.entity.Provincia;
import utn.frgp.pa2.bepart.entity.Ubicacion;
import utn.frgp.pa2.bepart.entity.Zona;
import utn.frgp.pa2.bepart.util.DBData;

public class BuscarEventosTask extends AsyncTask<Void, Integer, ArrayList<Evento>> {
    private static final String QUERY = "{call sp_BuscarEventos(?,?)}";
    private int categoriaInt;
    private int zonaInt;
    Context contexto;

    public BuscarEventosTask(int categoriaExt, int zonaExt, Context contexto){
        this.categoriaInt = categoriaExt;
        this.zonaInt = zonaExt;
        this.contexto = contexto;
    }
    @Override
    public ArrayList<Evento> doInBackground(Void... voids) {
        Evento evento_temp = null;
        ArrayList<Evento> resultado = new ArrayList<>();
        Connection connection = new DBData().getConnection();

        try {

            CallableStatement callableStatement = connection.prepareCall(QUERY);

            int i = 1;

            if(this.categoriaInt == 0){
                callableStatement.setInt(i++, 0);
            }else{
                callableStatement.setInt(i++, this.categoriaInt);
            }

            if(this.zonaInt == 0){
                callableStatement.setInt(i++, 0);
            }else{
                callableStatement.setInt(i++, this.zonaInt);
            }

            callableStatement.execute();
            ResultSet resultSet = callableStatement.getResultSet();
            while(resultSet.next()){
                evento_temp = new Evento();
                evento_temp.setIdEvento(resultSet.getInt("idEvento"));
                evento_temp.setTitulo(resultSet.getString("Titulo"));
                evento_temp.setDescripcion(resultSet.getString("Descripcion"));
                evento_temp.setUbicacion(new Ubicacion(resultSet.getInt("idUbicacion"),
                                            new Zona(resultSet.getInt("idZona"),
                                                new Provincia(resultSet.getInt("idProvincia"),
                                                    new Pais(resultSet.getInt("idPais"),resultSet.getString("Pais")),
                                                    resultSet.getString("Provincia")),
                                            resultSet.getString("DescripcionZona"), resultSet.getString("CodigoPostal")),
                                         resultSet.getString("Calle"),resultSet.getDouble("Latitud"), resultSet.getDouble("Longitud")));
                evento_temp.setFecha(resultSet.getString("Fecha"));
                evento_temp.setHora(resultSet.getString("Hora"));
                evento_temp.setCupos(resultSet.getInt("Cupos"));
                evento_temp.setImagen(resultSet.getString("Imagen"));
                evento_temp.setCategoria(new Categoria(resultSet.getInt("idCategoria"), resultSet.getString("Categoria")));
                evento_temp.setEstadoEvento(new EstadoEvento(resultSet.getInt("idEstadoEvento"),resultSet.getString("EstadoEvento")));
                evento_temp.setUsuarioOrg(resultSet.getString("UsuarioOrg"));
                resultado.add(evento_temp);
            }
            connection.close();
        }catch(Exception ex){
            ex.printStackTrace();
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return resultado;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(ArrayList<Evento> eventos) {
        //Toast.makeText(contexto,"Se Cargaron los Eventos Correctamente",Toast.LENGTH_SHORT).show();
    }
}
