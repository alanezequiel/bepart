/*package utn.frgp.pa2.bepart.model;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import utn.frgp.pa2.bepart.entity.Provincia;
import utn.frgp.pa2.bepart.entity.Ubicacion;
import utn.frgp.pa2.bepart.entity.Zona;
import utn.frgp.pa2.bepart.tasks.AltaUbicacionTask;
import utn.frgp.pa2.bepart.tasks.AltaZonaTask;
import utn.frgp.pa2.bepart.tasks.BuscarProvinciaTask;
import utn.frgp.pa2.bepart.tasks.BuscarUbicacionTask;
import utn.frgp.pa2.bepart.tasks.BuscarZonaTask;
import utn.frgp.pa2.bepart.util.IConnectable;


public class ZonaModel implements IConnectable<Zona> {

    @Override
    public boolean insert(Zona obj) throws ExecutionException, InterruptedException {
        boolean resp = false;
        if(obj != null){
            AltaZonaTask altaTask = new AltaZonaTask(obj);
            altaTask.execute();
            if(altaTask.get() != 0)
                resp = true;
            resp = false;
        }
        return resp;
    }

    @Override
    public boolean update(Zona obj) throws ExecutionException, InterruptedException {
        return false;
    }

    public ArrayList<Zona> select(Zona obj) throws ExecutionException, InterruptedException {
        /*if(obj != null){
            BuscarZonaTask buscarTask = new BuscarZonaTask(obj);
            buscarTask.execute();
            return buscarTask.get();
        }
        return null;
    }

}*/
