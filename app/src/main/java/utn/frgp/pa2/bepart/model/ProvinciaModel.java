/*package utn.frgp.pa2.bepart.model;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import utn.frgp.pa2.bepart.entity.Provincia;
import utn.frgp.pa2.bepart.tasks.AltaProvinciaTask;
import utn.frgp.pa2.bepart.tasks.BuscarProvinciaTask;
import utn.frgp.pa2.bepart.util.IConnectable;


public class ProvinciaModel implements IConnectable<Provincia> {

    @Override
    public boolean insert(Provincia obj) throws ExecutionException, InterruptedException {
        boolean resp = false;
        if(obj != null){
            AltaProvinciaTask altaTask = new AltaProvinciaTask(obj);
            altaTask.execute();
            if(altaTask.get() != 0)
                resp = true;
            resp = false;
        }
        return resp;
    }

    @Override
    public boolean update(Provincia obj) throws ExecutionException, InterruptedException {
        return false;
    }

    public ArrayList<Provincia> select(Provincia obj) throws ExecutionException, InterruptedException {
        if(obj != null){
            BuscarProvinciaTask buscarTask = new BuscarProvinciaTask(obj);
            buscarTask.execute();
            return buscarTask.get();
        }
        return null;
    }

}*/
