package utn.frgp.pa2.bepart.ui.organizador.eventos;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ExecutionException;

import utn.frgp.pa2.bepart.R;
import utn.frgp.pa2.bepart.Session.UbicacionesSession;
import utn.frgp.pa2.bepart.Session.UsuariosSession;
import utn.frgp.pa2.bepart.dialogos.DialogoDireccionEvento;
import utn.frgp.pa2.bepart.entity.Categoria;
import utn.frgp.pa2.bepart.entity.EstadoEvento;
import utn.frgp.pa2.bepart.entity.Evento;
import utn.frgp.pa2.bepart.entity.Provincia;
import utn.frgp.pa2.bepart.entity.Ubicacion;
import utn.frgp.pa2.bepart.entity.Usuario;
import utn.frgp.pa2.bepart.entity.Zona;
import utn.frgp.pa2.bepart.tasks.AltaEventoTask;
import utn.frgp.pa2.bepart.tasks.AltaProvinciaTask;
import utn.frgp.pa2.bepart.tasks.AltaUbicacionTask;
import utn.frgp.pa2.bepart.tasks.AltaZonaTask;
import utn.frgp.pa2.bepart.tasks.BuscarCategoriaTask;
import utn.frgp.pa2.bepart.tasks.BuscarProvinciaTask;
import utn.frgp.pa2.bepart.tasks.BuscarUbicacionTask;
import utn.frgp.pa2.bepart.tasks.BuscarZonaTask;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link NuevoEventoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NuevoEventoFragment extends Fragment {

    private static final String TAG = "NuevoEventoFragment";

    private static final String EVENTO = "evento";
    private static final String SESION = "sesion";
    private static final String ORGANIZADOR = "organizador";

    private Evento eventoEdicion = null;
    private UsuariosSession sesion = null;
    private Usuario organizador = null;

    public NuevoEventoFragment() {
        // Required empty public constructor
    }

    private TextView mDisplayDate,mDisplayTime,mDisplayMapa;
    private EditText tvTitulo,tvDescripcion,tvCupos;
    private CheckBox chkCupos;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    private TimePickerDialog.OnTimeSetListener mTimeSetListener;
    //private SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");
    //private SimpleDateFormat dh = new SimpleDateFormat("HH:mm aa");

    private UsuariosSession usuariosSession;
    private Usuario ssUser;
    private UbicacionesSession ubicacionesSession;
    private Ubicacion ssUbicacion;

    private Spinner spnCategoria;



    public static NuevoEventoFragment newInstance(Evento eventoSeleccionado) {
        NuevoEventoFragment fragment = new NuevoEventoFragment();
        Bundle args = new Bundle();
        args.putSerializable(EVENTO, eventoSeleccionado);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            eventoEdicion = (Evento) getArguments().getSerializable(EVENTO);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (container != null) {
            container.removeAllViews();
        }
        View view = inflater.inflate(R.layout.fragment_nuevo_evento, container, false);
        usuariosSession = new UsuariosSession(getContext());
        ssUser =  usuariosSession.getUserFromSessionData();
        ubicacionesSession = new UbicacionesSession(getContext());
        ubicacionesSession.clearUbicacionFromSession();

        if(eventoEdicion != null){
            Log.i(TAG, "onCreateView: EDICION! "+eventoEdicion.getTitulo());
        }

        Button btnGuardar = (Button) view.findViewById(R.id.btnGuardarNuevoEvento);
        mDisplayDate = (TextView) view.findViewById(R.id.tvDate);
        mDisplayTime = (TextView) view.findViewById(R.id.tvTime);
        mDisplayMapa = (TextView) view.findViewById(R.id.tvMapa);
        spnCategoria = (Spinner) view.findViewById(R.id.spinnerCategoria);
        tvTitulo = (EditText) view.findViewById(R.id.tvTitulo);
        tvDescripcion = (EditText) view.findViewById(R.id.tvDescripcion);
        tvCupos = (EditText) view.findViewById(R.id.tvCupos);
        chkCupos = (CheckBox) view.findViewById(R.id.chkCupos);


        ArrayList<Categoria> listaCategoria = new ArrayList<Categoria>();
        try {
            listaCategoria = obtenerCategorias();
            ArrayAdapter<Categoria> adpCategoria = new ArrayAdapter<Categoria>(getContext(), android.R.layout.simple_spinner_item,listaCategoria);
            adpCategoria.setDropDownViewResource(android.R.layout.simple_list_item_checked);
            spnCategoria.setAdapter(adpCategoria);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        //ACA ESTA EL DATEPICKER
        mDisplayDate.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dpDialog = new DatePickerDialog(view.getContext(),
                        android.R.style.Theme_Holo_Dialog_MinWidth,mDateSetListener,year,month,day);
                dpDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                cal.add(Calendar.DAY_OF_MONTH, +2);
                dpDialog.getDatePicker().setMinDate(cal.getTimeInMillis());
                dpDialog.show();
            }
        });
        //DATOS DEL DATEPICKER LOS MANDAMOS AL TEXTVIEW
        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Log.d("SelectDate","onDateSet: date: " + year + "/" + (month+1) + "/" + dayOfMonth);

                String dateSelected = dayOfMonth + "-" + (month+1) + "-" + year;
                mDisplayDate .setText(dateSelected);
            }
        };

        //ACA ESTA EL TIMEPICKER
        mDisplayTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerDialog tpDialog = new TimePickerDialog(view.getContext(),
                        android.R.style.Theme_Holo_Dialog_MinWidth,mTimeSetListener,12,0,true);
                tpDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                tpDialog.show();
            }
        });
        //DATOS DEL TIMEPICKET LOS MANDAMOS AL TEXTVIEW
        mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                int hora = hourOfDay;
                int min = minute;

                String time = hora + ":" + min;
                SimpleDateFormat f24Hours = new SimpleDateFormat("HH:mm");
                try {
                    Date date = f24Hours.parse(time);
                    SimpleDateFormat f12Hours = new SimpleDateFormat("HH:mm");
                    mDisplayTime.setText(f12Hours.format(date));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        };

        mDisplayMapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(view.getContext(),"Click Boton Mapa",Toast.LENGTH_LONG).show();
                new DialogoDireccionEvento(view.getContext());

            }
        });


        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ubicacionesSession = new UbicacionesSession(getContext());
                ssUbicacion = ubicacionesSession.getUbicacionFromSessionData();
                Categoria cs= (Categoria) spnCategoria.getSelectedItem();

                if(tvTitulo.getText().toString().equals("")) {
                    Toast.makeText(getContext(), "Debe Escribir un Titulo", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(tvDescripcion.getText().toString().equals("")) {
                    Toast.makeText(getContext(), "Debe Escribir una Descripcion", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(tvCupos.getText().toString().equals("") || tvCupos.getText().toString().equals("0")) {
                    Toast.makeText(getContext(), "Debe Definir los Cupos", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(mDisplayDate.getText().toString().equals("Seleccionar\nFecha") || mDisplayTime.getText().toString().equals("Seleccionar\nHora")) {
                    Toast.makeText(getContext(), "Debe Seleccionar la Fecha y Hora del Evento", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(cs.getIdCategoria() == -1){
                    Toast.makeText(getContext(),"Debe Seleccionar la Categoria del Evento",Toast.LENGTH_SHORT).show();
                    return;
                }
                if (ssUbicacion == null){
                    Toast.makeText(getContext(),"Debe Seleccionar la Ubicacion del Evento",Toast.LENGTH_SHORT).show();
                    return;
                }else {
                    /*Toast.makeText(getContext(), "Calle : " + ssUbicacion.getCalle() + "\n Localidad :" +
                            ssUbicacion.getZona().getDescripcionZona(), Toast.LENGTH_SHORT).show();

                    //cuando termina de cargar
                    ubicacionesSession.clearUbicacionFromSession();*/
                    try {
                        CargarEvento(ssUbicacion,cs);
                        ubicacionesSession.clearUbicacionFromSession();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }
        });
        //dialogo.show();
        return view;
    }

    public void CargarEvento(Ubicacion ssUbicacion,Categoria cs) throws ExecutionException, InterruptedException {
        Ubicacion ubi_temp = ssUbicacion;

        ArrayList<Provincia> prov = new ArrayList<Provincia>();
        BuscarProvinciaTask buscarProvinciaTask = new BuscarProvinciaTask(0,ssUbicacion.getZona().getProvicia().getDescripcion());
        buscarProvinciaTask.execute();
        prov = buscarProvinciaTask.get();
        if(prov.size() == 0){
            AltaProvinciaTask altaProvinciaTask = new AltaProvinciaTask(ssUbicacion.getZona().getProvicia());
            altaProvinciaTask.execute();
            if(altaProvinciaTask.get() == 0){
                Toast.makeText(getContext(),"Error al Cargar la Provincia",Toast.LENGTH_SHORT).show();
                return;
            }
            buscarProvinciaTask = new BuscarProvinciaTask(0,ssUbicacion.getZona().getProvicia().getDescripcion());
            buscarProvinciaTask.execute();
            prov = buscarProvinciaTask.get();
        }

        ubi_temp.setZona(new Zona(0,prov.get(0),ssUbicacion.getZona().getDescripcionZona(),ssUbicacion.getZona().getCodigoPostal()));

        ArrayList<Zona> zona = new ArrayList<Zona>();
        BuscarZonaTask buscarZonaTask = new BuscarZonaTask(0,ubi_temp.getZona().getDescripcionZona());
        buscarZonaTask.execute();
        zona = buscarZonaTask.get();
        if(zona.size() == 0){
            AltaZonaTask altaZonaTask = new AltaZonaTask(ubi_temp.getZona());
            altaZonaTask.execute();
            if(altaZonaTask.get() == 0){
                Toast.makeText(getContext(),"Error al Cargar la Zona",Toast.LENGTH_SHORT).show();
                return;
            }
            buscarZonaTask = new BuscarZonaTask(0,ubi_temp.getZona().getDescripcionZona());
            buscarZonaTask.execute();
            zona = buscarZonaTask.get();
                if(zona.size() == 0){
                    Toast.makeText(getContext(),"Error al Cargar la Zona 2",Toast.LENGTH_SHORT).show();
                    return;
                }
        }


        ssUbicacion.setZona(zona.get(0));
        AltaUbicacionTask altaUbicacionTask = new AltaUbicacionTask(ssUbicacion);
        altaUbicacionTask.execute();
        if(altaUbicacionTask.get() == 0){
            Toast.makeText(getContext(),"Error al Cargar la Ubicacion",Toast.LENGTH_SHORT).show();
            return;
        }

        ArrayList<Ubicacion> nUbicacion = new ArrayList<Ubicacion>();
        BuscarUbicacionTask buscarUbicacionTask = new BuscarUbicacionTask(0,ssUbicacion.getCalle());
        buscarUbicacionTask.execute();
        nUbicacion = buscarUbicacionTask.get();
        if(nUbicacion.get(0) == null){
            Toast.makeText(getContext(),"Error al Buscar la Ubicacion",Toast.LENGTH_SHORT).show();
            return;
        }

        Evento nEvento = new Evento();
        nEvento.setTitulo(tvTitulo.getText().toString());
        nEvento.setDescripcion(tvDescripcion.getText().toString());
        nEvento.setUbicacion(nUbicacion.get(0));
        nEvento.setCategoria(cs);
        nEvento.setFecha(mDisplayDate.getText().toString());
        nEvento.setHora(mDisplayTime.getText().toString());
        nEvento.setCupos(Integer.parseInt(tvCupos.getText().toString()));
        nEvento.setEstadoEvento(new EstadoEvento(1,"Pendiente"));
        nEvento.setUsuarioOrg(ssUser.getUsername());

        AltaEventoTask altaEventoTask = new AltaEventoTask(nEvento,ssUser.getIdUsuario());
        altaEventoTask.execute();
        if(altaEventoTask.get() == 0){
            Toast.makeText(getContext(),"Error al Cargar el Evento",Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(getContext(),"Evento Cargado Correctamente",Toast.LENGTH_LONG).show();
            tvTitulo.setText("");
            tvDescripcion.setText("");
            tvCupos.setText("");
        }

        //Toast.makeText(getContext(),"Se paso joya",Toast.LENGTH_SHORT).show();
    }

    
    public void show(FragmentManager supportFragmentManager, String asd) {
    }

    public ArrayList<Categoria> obtenerCategorias() throws ExecutionException, InterruptedException {
        ArrayList<Categoria> categoria = new ArrayList<>();
        ArrayList<Categoria> resultado = null;
        BuscarCategoriaTask buscarCategoriaTask = new BuscarCategoriaTask(0);
        buscarCategoriaTask.execute();
        resultado = buscarCategoriaTask.get();

        categoria.add(new Categoria(-1,"Seleccionar"));

        for (Categoria _categoria : resultado) {
            categoria.add(_categoria);
        }

        return categoria;
    }
}