package utn.frgp.pa2.bepart.ui.moderador;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import utn.frgp.pa2.bepart.R;
import utn.frgp.pa2.bepart.Session.Session;
import utn.frgp.pa2.bepart.Session.SessionData;
import utn.frgp.pa2.bepart.Session.UsuariosSession;
import utn.frgp.pa2.bepart.entity.AsistenciaxEvento;
import utn.frgp.pa2.bepart.entity.Evento;
import utn.frgp.pa2.bepart.entity.Usuario;
import utn.frgp.pa2.bepart.tasks.AltaAsistenciaxEventoTask;
import utn.frgp.pa2.bepart.tasks.BuscarAsistenciaEventoTask;
import utn.frgp.pa2.bepart.ui.eventos.DenunciarEvento;

public class DetalleEventoDenunciadoFragment extends Fragment {

    private static final String TAG = "DetalleEventoDenunciadoFragment";

    private static final String EVENTO_SELECCIONADO = "seleccionado";

    private Evento mEvento;

    public DetalleEventoDenunciadoFragment() {
        // Required empty public constructor
    }

    public static DetalleEventoDenunciadoFragment newInstance(Evento seleccionado) {
        DetalleEventoDenunciadoFragment fragment = new DetalleEventoDenunciadoFragment();
        Bundle args = new Bundle();
        args.putSerializable(EVENTO_SELECCIONADO,seleccionado);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            //mParam2 = getArguments().getString(ARG_PARAM2);
            mEvento = (Evento) getArguments().getSerializable(EVENTO_SELECCIONADO);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_detalle_evento, container, false);

        UsuariosSession usuariosSession = new UsuariosSession(getContext());
        Usuario ssUser =  usuariosSession.getUserFromSessionData();


        Button btnParticipar = root.findViewById(R.id.btnParticipar);
        Button btnDenunciar = root.findViewById(R.id.btnDenunciar);
        TextView tvTitulo = root.findViewById(R.id.tvTitulo);
        TextView tvDetalle = root.findViewById(R.id.tvDetalle);
        TextView tvOrganizador = root.findViewById(R.id.tvOrganizador);
        TextView tvLugar = root.findViewById(R.id.tvLugar);
        TextView tvFecha = root.findViewById(R.id.tvFechayHora);
        TextView tvDisponible = root.findViewById(R.id.tvCupoEvento);
        TextView tvMensaje = root.findViewById(R.id.tvMensajeDetalle);
        TextView tvCategoria = root.findViewById(R.id.tvCategoria);
        String Lugar = mEvento.getUbicacion().getCalle() + " - " + mEvento.getUbicacion().getZona().getDescripcionZona();
        if(mEvento.getUbicacion().getZona().getCodigoPostal() != null){
            Lugar = Lugar + "(" + mEvento.getUbicacion().getZona().getCodigoPostal() + ")";
        }
        Lugar = Lugar + "\n" + mEvento.getUbicacion().getZona().getProvicia().getDescripcion() + " ," + mEvento.getUbicacion().getZona().getProvicia().getPais().getDescripcion();

        tvTitulo.setText(mEvento.getTitulo());
        tvDetalle.setText(mEvento.getDescripcion());
        tvOrganizador.setText(mEvento.getUsuarioOrg());
        tvLugar.setText(Lugar);

        String dateUsed = mEvento.getFecha();
        String[] parts = dateUsed.split("-");
        String year = parts[0];
        String month = parts[1];
        String day = parts[2];

        String nuevaFecha = day + "/" + month + "/" + year;

        String timeUsed = mEvento.getHora();
        String[] partTime = timeUsed.split(":");
        String hour = partTime[0];
        String minute = partTime[1];
        String second = partTime[2];

        String nuevaHora = hour + ":" + minute;

        tvFecha.setText(nuevaFecha + " " + nuevaHora + " hs");
        tvDisponible.setText(mEvento.getCupos() + " Disponibles");
        tvCategoria.setText(mEvento.getCategoria().getDescripcion());

        if(ssUser == null){
            btnParticipar.setVisibility(View.INVISIBLE);
            btnDenunciar.setVisibility(View.INVISIBLE);
            tvMensaje.setVisibility(View.VISIBLE);
            tvMensaje.setText("Debe Iniciar Sesion para Participar en los Eventos");
        }

        /*showSelectedFragment(new explorarmapaFragment()); //ACA VAN LOS DATOS DE LA UBICACION EN EL CONSTRUCTOR.*/

        btnParticipar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int val = mEvento.getCupos() - 1;
                ArrayList<AsistenciaxEvento> resultado = null;
                BuscarAsistenciaEventoTask buscarAsistenciaEventoTask = new BuscarAsistenciaEventoTask(ssUser,mEvento.getIdEvento());
                buscarAsistenciaEventoTask.execute();
                try {
                    resultado = buscarAsistenciaEventoTask.get();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                if( resultado.size() > 0){
                    Toast.makeText(getContext(),"Ya esta Participando en " + resultado.get(0).getEvento().getTitulo(),Toast.LENGTH_LONG).show();
                }
                else{
                    if(val < 0){
                        Toast.makeText(getContext(),"No hay Cupos Disponibles!",Toast.LENGTH_SHORT).show();
                        return;
                    }
                    AltaAsistenciaxEventoTask altaAsistenciaTask = new AltaAsistenciaxEventoTask(mEvento.getIdEvento(),ssUser.getIdUsuario());
                    altaAsistenciaTask.execute();
                    try {
                        if(altaAsistenciaTask.get() != 0){
                            Toast.makeText(getContext(),"¡Gracias por Participar!",Toast.LENGTH_LONG).show();
                            int c = mEvento.getCupos() - 1 ;
                            tvDisponible.setText(c + " Disponibles");
                        }else {
                            Toast.makeText(getContext(),"¡No se pudo grabar su participacion\nintente mas tarde!",Toast.LENGTH_LONG).show();
                        }
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        btnDenunciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DenunciarEvento denunciarEvento = new DenunciarEvento();
                Session session = new Session(getContext(), SessionData.GeneralSessionFile);
                session.putSessionData("IdUsuario", ssUser.getIdUsuario());
                session.putSessionData("IdEvento", mEvento.getIdEvento());
                denunciarEvento.show(getActivity().getSupportFragmentManager(), "DenunciaEvento");
            }
        });
        return root;
    }


}