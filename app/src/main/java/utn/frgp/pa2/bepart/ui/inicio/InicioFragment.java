package utn.frgp.pa2.bepart.ui.inicio;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import utn.frgp.pa2.bepart.R;
import utn.frgp.pa2.bepart.ui.explorarlupa.explorarlupaFragment;
import utn.frgp.pa2.bepart.ui.explorarmapa.explorarmapaFragment;
import utn.frgp.pa2.bepart.ui.organizador.eventos.NuevoEventoFragment;

public class InicioFragment extends Fragment {

    private InicioViewModel inicioViewModel;
    private BottomNavigationView mBottonNavigation;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        if (container != null) {
            container.removeAllViews();
        }
        View root = inflater.inflate(R.layout.fragment_inicio, container, false);
        showSelectedFragment(new explorarlupaFragment()); //Este es el default cuando carga
        mBottonNavigation = (BottomNavigationView) root.findViewById(R.id.bottomNavigation);
        mBottonNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                if (item.getItemId() == R.id.menu_lupa){
                    showSelectedFragment(new explorarlupaFragment());
                }

                if (item.getItemId() == R.id.menu_mapa){
                    showSelectedFragment(new explorarmapaFragment());
                }

                return true;
            }
        });

        return root;
    }

    private void showSelectedFragment(Fragment fragment){
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container_ini,fragment).setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).commit();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                //nada.
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);
    }

    /*private void newEventTest(){
        NuevoEventoFragment nuevoEventoFragment = new NuevoEventoFragment();
       // nuevoEventoFragment.setTargetFragment(InicioFragment.this, 1);
        nuevoEventoFragment.show(getActivity().getSupportFragmentManager(),"asd");

    }*/

}