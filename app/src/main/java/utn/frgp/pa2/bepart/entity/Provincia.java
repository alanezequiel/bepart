package utn.frgp.pa2.bepart.entity;

public class Provincia {
    private int idProvincia;
    private Pais pais;
    private String descripcion;

    public Provincia(){};

    public Provincia(int idProvincia, Pais pais, String descripcion) {
        this.idProvincia = idProvincia;
        this.pais = pais;
        this.descripcion = descripcion;
    }

    public int getIdProvincia() {
        return idProvincia;
    }

    public Pais getPais() {
        return pais;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setIdProvincia(int idProvincia) {
        this.idProvincia = idProvincia;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
