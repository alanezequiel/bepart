package utn.frgp.pa2.bepart.ui.eventos;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import utn.frgp.pa2.bepart.R;
import utn.frgp.pa2.bepart.Session.Session;
import utn.frgp.pa2.bepart.Session.SessionData;
import utn.frgp.pa2.bepart.tasks.AltaDenunciaTask;

public class DenunciarEvento extends DialogFragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_denunciar_evento, container, false);

        Button enviar = view.findViewById(R.id.enviar);

        List<String> spinnerList = new ArrayList<String>();

        spinnerList.add("Otro");
        spinnerList.add("Evento Falso");
        spinnerList.add("Violencia");
        spinnerList.add("Lenguaje Inapropiado");
        spinnerList.add("Spam");
        spinnerList.add("Actividad Sexual");
        spinnerList.add("Burla hacia victimas");
        spinnerList.add("Promoción del consumo de Drogas");
        spinnerList.add("Ventas no Autorizadas");
        spinnerList.add("Propiedad Intelectual");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                getContext(), android.R.layout.simple_spinner_item, spinnerList);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner spinner = (Spinner) view.findViewById(R.id.spinner);
        spinner.setAdapter(adapter);

        enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText editText = v.findViewById(R.id.comentario);
                Session session = new Session(getContext(), SessionData.GeneralSessionFile);
                int idEvento = session.getIntSessionData("IdEvento");
                int idUsuario = session.getIntSessionData("IdUsuario");
                int tipoDenuncia = spinner.getSelectedItemPosition();
                EditText etComentario = view.findViewById(R.id.comentario);
                AltaDenunciaTask altaDenunciaTask = new AltaDenunciaTask(idEvento, idUsuario, tipoDenuncia, etComentario.getText().toString());
                altaDenunciaTask.execute();
                try {
                    if(altaDenunciaTask.get() != 0){
                        Toast.makeText(getContext(), "Denuncia registrada correctamente", Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(getContext(), "Denuncia ya registrada previamente", Toast.LENGTH_SHORT).show();
                    }
                } catch (ExecutionException | InterruptedException e) {
                    e.printStackTrace();
                }
                getDialog().dismiss();
            }
        });
        return view;
    }
}
