package utn.frgp.pa2.bepart.ui.registro;

import android.os.Bundle;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.regex.Pattern;

import utn.frgp.pa2.bepart.R;
import utn.frgp.pa2.bepart.entity.EstadoUsuario;
import utn.frgp.pa2.bepart.entity.TipoUsuario;
import utn.frgp.pa2.bepart.entity.Usuario;
import utn.frgp.pa2.bepart.model.UsuariosModel;
import utn.frgp.pa2.bepart.tasks.BuscarUsuarioTask;
import utn.frgp.pa2.bepart.ui.login.Login;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link RegistroFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RegistroFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private View view;

    public RegistroFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment RegistroFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static RegistroFragment newInstance(String param1, String param2) {
        RegistroFragment fragment = new RegistroFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    FloatingActionButton fab;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


    }

    private boolean validarEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (container != null) {
            container.removeAllViews();
        }

        View view = inflater.inflate(R.layout.fragment_registro, container, false);

        EditText username = view.findViewById(R.id.editUsername);
        EditText nombre = view.findViewById(R.id.editNombre);
        EditText apellido = view.findViewById(R.id.editApellido);
        EditText password2 = view.findViewById(R.id.editPassword2);
        EditText email = view.findViewById(R.id.editEmail);
        EditText password = view.findViewById(R.id.editPassword);

        Button btnAlta = (Button) view.findViewById(R.id.btnAltaUsuario);
        btnAlta.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String usernameText = username.getText().toString();
                        String nombreText = nombre.getText().toString();
                        String apellidoText = apellido.getText().toString();
                        String emailText = email.getText().toString();
                        String passwordText = password.getText().toString();
                        String passwordText2 = password2.getText().toString();

                        String msj = "";

                        if( usernameText.isEmpty() ||nombreText.isEmpty() || apellidoText.isEmpty()
                                || emailText.isEmpty() || passwordText.isEmpty() || passwordText2.isEmpty() ){
                            msj = "Faltan completar campos obligatorios";
                        }else if( ! passwordText.equals( passwordText2 ) ){
                            msj = "Las contraseñas no coinciden";
                        }else if (!validarEmail(emailText)){
                            msj = "Email invalido";
                        }

                        if(msj.length() == 0){
                            TipoUsuario tipoUsuario = new TipoUsuario();
                            EstadoUsuario estadoUsuario = new EstadoUsuario();

                            Usuario usuario = new Usuario(
                                    usernameText,
                                    nombreText,
                                    apellidoText,
                                    "",
                                    emailText,
                                    passwordText,
                                    estadoUsuario,
                                    0,
                                    tipoUsuario
                            );

                            try {
                                ArrayList<Usuario> resultado = null;
                                BuscarUsuarioTask buscarUsuarioTask = new BuscarUsuarioTask(usuario);
                                buscarUsuarioTask.execute();
                                resultado = buscarUsuarioTask.get();
                                
                                if (resultado.size() == 0)
                                {
                                    UsuariosModel usuariosModel = new UsuariosModel();
                                    usuariosModel.insert(usuario);
                                    msj = "Usuario creado correctamente";
                                    username.setText("");
                                    nombre.setText("");
                                    apellido.setText("");
                                    email.setText("");
                                    password.setText("");
                                    password2.setText("");
                                }
                                else {
                                    String errmsj = "";
                                    for (Usuario _usuario : resultado
                                         )
                                    {
                                        if(usernameText.equals(_usuario.getUsername()) && emailText.equals(_usuario.getEmail()) && _usuario.getActivo()==1)
                                        {
                                            errmsj = "Username y Email utilizado con anterioridad";
                                        }else if (usernameText.equals(_usuario.getUsername()) && _usuario.getActivo()==1)
                                        {
                                            errmsj = "Username utilizado con anterioridad";
                                        }
                                        else if (emailText.equals(_usuario.getEmail()) && _usuario.getActivo()==1)
                                        {
                                            errmsj = "Email utilizado con anterioridad";
                                        }
                                        Toast.makeText(view.getContext(), errmsj, errmsj.length()).show();
                                    }                                    
                                }                                

                            } catch (ExecutionException | InterruptedException e) {
                                e.printStackTrace();
                            }

                        }
                        Toast.makeText(view.getContext(), msj, msj.length()).show();
                        if (msj=="Usuario creado correctamente"){
                            getActivity().getSupportFragmentManager().beginTransaction().replace(
                                    R.id.nav_host_fragment,new Login()).setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).commit();
                        }

                    }
                }
        );
        return view;
    }

}