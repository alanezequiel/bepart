package utn.frgp.pa2.bepart.entity;

public class Zona {
    private int idZona;
    private Provincia Provicia;
    private String descripcionZona;
    private String codigoPostal;

    public Zona() {
    }

    public Zona(int idZona, Provincia provicia, String descripcionZona, String codigoPostal) {
        this.idZona = idZona;
        Provicia = provicia;
        this.descripcionZona = descripcionZona;
        this.codigoPostal = codigoPostal;
    }

    public int getIdZona() {
        return idZona;
    }

    public void setIdZona(int idZona) {
        this.idZona = idZona;
    }

    public Provincia getProvicia() {
        return Provicia;
    }

    public void setProvicia(Provincia provicia) {
        Provicia = provicia;
    }

    public String getDescripcionZona() {
        return descripcionZona;
    }

    public void setDescripcionZona(String descripcionZona) {
        this.descripcionZona = descripcionZona;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String toString() {
        return descripcionZona;
    }
}
