package utn.frgp.pa2.bepart.entity;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

public class Evento implements Serializable {
    private int idEvento;
    private String titulo;
    private String descripcion;
    private Ubicacion ubicacion;
    private String fecha;
    private String hora;
    private int cupos;
    private String imagen;
    private Categoria categoria;
    private EstadoEvento estadoEvento;
    private String usuarioOrg;
    private float calificacion;

    public Evento() {

    }

    public Evento(int idEvento, String titulo, String descripcion, Ubicacion ubicacion,
                  String fecha, String hora, int cupos, String imagen, Categoria categoria, EstadoEvento estadoEvento, String usuarioOrg, float _calificacion) {
        this.idEvento = idEvento;
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.ubicacion = ubicacion;
        this.fecha = fecha;
        this.hora = hora;
        this.cupos = cupos;
        this.imagen = imagen;
        this.categoria = categoria;
        this.estadoEvento = estadoEvento;
        this.usuarioOrg = usuarioOrg;
        this.calificacion = _calificacion;
    }

    public int getIdEvento() {
        return idEvento;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public Ubicacion getUbicacion() {
        return ubicacion;
    }

    public String getFecha() {
        return fecha;
    }

    public String getHora() {
        return hora;
    }

    public int getCupos() {
        return cupos;
    }

    public String getImagen() {
        return imagen;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public EstadoEvento getEstadoEvento() {
        return estadoEvento;
    }

    public String getUsuarioOrg() {
        return usuarioOrg;
    }

    public void setIdEvento(int idEvento) {
        this.idEvento = idEvento;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setUbicacion(Ubicacion ubicacion) {
        this.ubicacion = ubicacion;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public void setCupos(int cupos) {
        this.cupos = cupos;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public void setEstadoEvento(EstadoEvento estadoEvento) {
        this.estadoEvento = estadoEvento;
    }

    public void setUsuarioOrg(String usuarioOrg) {
        this.usuarioOrg = usuarioOrg;
    }

    public float getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(float calificacion) {
        this.calificacion = calificacion;
    }
}
