package utn.frgp.pa2.bepart.model;

import java.util.concurrent.ExecutionException;

import utn.frgp.pa2.bepart.entity.CalificacionEvento;
import utn.frgp.pa2.bepart.entity.Documentacion;
import utn.frgp.pa2.bepart.tasks.AltaCalificacionEventoTask;
import utn.frgp.pa2.bepart.tasks.AltaDocumentacionTask;
import utn.frgp.pa2.bepart.tasks.BuscarCalificacionEventoTask;

public class CalificacionesEventosModel {

    public CalificacionEvento get(CalificacionEvento calificacionEvento) throws ExecutionException, InterruptedException {
        if(calificacionEvento == null){
            calificacionEvento = new CalificacionEvento(0,0,0,"", 0, null);
        }

        BuscarCalificacionEventoTask buscarCalificacionEventoTask = new BuscarCalificacionEventoTask(calificacionEvento.getIdUsuario(), calificacionEvento.getIdEvento());
        buscarCalificacionEventoTask.execute();
        return buscarCalificacionEventoTask.get();

    }

    public boolean insert(CalificacionEvento obj) throws ExecutionException, InterruptedException {
        boolean resp = false;
        if(obj != null){
            AltaCalificacionEventoTask altaTask = new AltaCalificacionEventoTask(obj);
            altaTask.execute();
            if(altaTask.get() != 0)
                resp = true;
            resp = false;
        }
        return resp;
    }
}
