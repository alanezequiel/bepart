package utn.frgp.pa2.bepart.ui.eventos;

import android.app.Activity;
import android.content.ContentProvider;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import utn.frgp.pa2.bepart.R;
import utn.frgp.pa2.bepart.Session.UsuariosSession;
import utn.frgp.pa2.bepart.entity.AsistenciaxEvento;
import utn.frgp.pa2.bepart.entity.CalificacionEvento;
import utn.frgp.pa2.bepart.entity.Evento;
import utn.frgp.pa2.bepart.entity.Usuario;
import utn.frgp.pa2.bepart.interfaces.IComunicaFragments;
import utn.frgp.pa2.bepart.model.CalificacionesEventosModel;
import utn.frgp.pa2.bepart.recyclerview.RecyclerViewAdaptadorMisEventos;
import utn.frgp.pa2.bepart.tasks.BuscarAsistenciaEventoTask;
import utn.frgp.pa2.bepart.tasks.BuscarMisAyETask;
import utn.frgp.pa2.bepart.ui.explorarlupa.explorarlupaFragment;

public class MisEventosFragment extends Fragment implements RecyclerViewAdaptadorMisEventos.IEventoClickListener  {

    private static final String TAG = "MisEventosFragment";
    private RecyclerView recyclerViewMisEventos;
    private RecyclerViewAdaptadorMisEventos adaptadorMisEventos;

    private UsuariosSession usuariosSession;
    private Usuario ssUser;

    private Activity activity;
    private IComunicaFragments mComunicaFragments;

    //private Evento eventoSeleccionado;

    public MisEventosFragment (){

    }

    public static MisEventosFragment newInstance(String param1, String param2) {
        MisEventosFragment fragment = new MisEventosFragment();
        return fragment;
    }


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        if (container != null) {
            container.removeAllViews();
        }

        View root = inflater.inflate(R.layout.fragment_mis_eventos, container, false);
        usuariosSession = new UsuariosSession(getContext());
        ssUser =  usuariosSession.getUserFromSessionData();

        recyclerViewMisEventos = (RecyclerView) root.findViewById(R.id.rvMisEventos);
        recyclerViewMisEventos.setLayoutManager(new LinearLayoutManager(getContext()));

        switch (ssUser.getTipoUsuario().getDescripcion().toLowerCase()){
            case "organizador":
                    try {
                        adaptadorMisEventos = new RecyclerViewAdaptadorMisEventos(obtenerMisAyE(),ssUser,getContext(), this, this);
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                break;

            case "usuario":
                    try {
                        adaptadorMisEventos = new RecyclerViewAdaptadorMisEventos(obtenerEventosAsistidos(),ssUser,getContext(), this, this);
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                break;
        }
        recyclerViewMisEventos.setAdapter(adaptadorMisEventos);

        return root;
    }

    public List<Evento> obtenerEventosAsistidos() throws ExecutionException, InterruptedException {
        List<Evento> Evento = new ArrayList<>();

        ArrayList<AsistenciaxEvento> resultado = null;
        BuscarAsistenciaEventoTask buscarAsistenciaEventoTask = new BuscarAsistenciaEventoTask(ssUser,0);
        buscarAsistenciaEventoTask.execute();
        resultado = buscarAsistenciaEventoTask.get();

        for (AsistenciaxEvento _evento : resultado) {
            Evento.add(_evento.getEvento());
        }
        return Evento;
    }


    public List<Evento> obtenerMisAyE() throws ExecutionException, InterruptedException {
        List<Evento> Evento = new ArrayList<>();

        ArrayList<Evento> resultado = null;
        BuscarMisAyETask buscarMisAyETask = new BuscarMisAyETask(ssUser.getIdUsuario());
        buscarMisAyETask.execute();
        resultado = buscarMisAyETask.get();

        for (Evento _evento : resultado) {
            Evento.add(_evento);
        }
        return Evento;
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if(context instanceof Activity){
            activity = (Activity) context;
            mComunicaFragments = (IComunicaFragments) activity;
        }

    }

    @Override
    public void onEventoClick(int position) {
        //Evento eventoSeleccionado = adaptadorMisEventos.getItemAtIndexPosition(position);
        //mComunicaFragments.MostrarDatosEdicionEvento(eventoSeleccionado);
    }

    @Override
    public void onFinalzarEventoClick(int position) {
        switch (ssUser.getTipoUsuario().getDescripcion().toLowerCase()){
            case "organizador":
                try {
                    adaptadorMisEventos = new RecyclerViewAdaptadorMisEventos(obtenerMisAyE(),ssUser,getContext(), this, this);
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                break;

            case "usuario":
                try {
                    adaptadorMisEventos = new RecyclerViewAdaptadorMisEventos(obtenerEventosAsistidos(),ssUser,getContext(), this, this);
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                break;
        }
        recyclerViewMisEventos.setAdapter(adaptadorMisEventos);
    }

    public void calificarEvento(Evento eventoSeleccionado, String comentario, int rating){

        CalificacionesEventosModel model = new CalificacionesEventosModel();
        boolean result = false;
        try {

            CalificacionEvento calificacionEvento = new CalificacionEvento();

            calificacionEvento.setCalificacion(rating);
            calificacionEvento.setComentario(comentario);
            calificacionEvento.setIdUsuario(ssUser.getIdUsuario());
            calificacionEvento.setIdEvento(eventoSeleccionado.getIdEvento());

            result = model.insert(calificacionEvento);

            Log.e(TAG, "calificarEvento: "+result);

        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Toast.makeText(getContext(),"Gracias por su Calificacion!",Toast.LENGTH_SHORT).show();
        // TODO: 14/11/2020 enviar a la base de datos
        Log.e(TAG, "calificarEvento: "+eventoSeleccionado.getTitulo() + " . " + comentario + " . " + rating);
    }

    public void denunciarEvento(Evento eventoSeleccionado){
        Log.e(TAG, "calificarEvento: "+eventoSeleccionado.getTitulo());
    }

}