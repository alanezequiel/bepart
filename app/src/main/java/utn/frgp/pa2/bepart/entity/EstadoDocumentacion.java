package utn.frgp.pa2.bepart.entity;

import java.io.Serializable;

public class EstadoDocumentacion implements Serializable {

    private int idEstadoDoc;
    private String descripcion;
    private int estado;

    public static final int SIN_CARGAR = 6;
    public static final int PENDIENTE_APROBACION = 7;
    public static final int APROBADA = 8;
    public static final int RECHAZADA = 9;
    public static final int RECHAZADA_DEFINITIVA = 10;

    public EstadoDocumentacion(){}

    public EstadoDocumentacion(int idEstadoDoc, String descripcion, int estado) {
        this.idEstadoDoc = idEstadoDoc;
        this.descripcion = descripcion;
        this.estado = estado;
    }

    public int getIdEstadoDoc() {
        return idEstadoDoc;
    }

    public void setIdEstadoDoc(int idEstadoDoc) {
        this.idEstadoDoc = idEstadoDoc;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

}
