package utn.frgp.pa2.bepart.tasks;

import android.os.AsyncTask;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import utn.frgp.pa2.bepart.entity.CalificacionEvento;
import utn.frgp.pa2.bepart.util.DBData;

public class AltaCalificacionEventoTask extends AsyncTask<Void, String, Integer> {

    private static final String QUERY ="{call sp_RegistrarCalificacionEvento(?,?,?,?)}";
    private CalificacionEvento calificacionEvento;

    public AltaCalificacionEventoTask(CalificacionEvento calificacionEvento){
        this.calificacionEvento = calificacionEvento;
    }


    @Override
    protected Integer doInBackground(Void... voids) {
        Integer temp = new Integer(0);
        Connection connection = new DBData().getConnection();
        try{

            CallableStatement preparedStatement = connection.prepareCall(QUERY);

            int i = 1;

            preparedStatement.setInt(i++,this.calificacionEvento.getIdEvento());
            preparedStatement.setInt(i++,this.calificacionEvento.getIdUsuario());
            preparedStatement.setString(i++,this.calificacionEvento.getComentario());
            preparedStatement.setInt(i++,(int)(this.calificacionEvento.getCalificacion()));
            temp = preparedStatement.executeUpdate();
            connection.close();
        }catch(Exception ex){
            ex.printStackTrace();
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return temp;
    }
}
