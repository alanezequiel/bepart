/*package utn.frgp.pa2.bepart.model;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import utn.frgp.pa2.bepart.entity.Provincia;
import utn.frgp.pa2.bepart.entity.Ubicacion;
import utn.frgp.pa2.bepart.tasks.AltaUbicacionTask;
import utn.frgp.pa2.bepart.tasks.BuscarProvinciaTask;
import utn.frgp.pa2.bepart.tasks.BuscarUbicacionTask;
import utn.frgp.pa2.bepart.util.IConnectable;


public class UbicacionModel implements IConnectable<Ubicacion> {

    @Override
    public boolean insert(Ubicacion obj) throws ExecutionException, InterruptedException {
        boolean resp = false;
        if(obj != null){
            AltaUbicacionTask altaTask = new AltaUbicacionTask(obj);
            altaTask.execute();
            if(altaTask.get() != 0)
                resp = true;
            resp = false;
        }
        return resp;
    }

    @Override
    public boolean update(Ubicacion obj) throws ExecutionException, InterruptedException {
        return false;
    }

    public ArrayList<Ubicacion> select(Ubicacion obj) throws ExecutionException, InterruptedException {
        if(obj != null){
            BuscarUbicacionTask buscarTask = new BuscarUbicacionTask(obj);
            buscarTask.execute();
            return buscarTask.get();
        }
        return null;
    }

}*/
