package utn.frgp.pa2.bepart.tasks;

import android.os.AsyncTask;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import utn.frgp.pa2.bepart.entity.Documentacion;
import utn.frgp.pa2.bepart.entity.EstadoDocumentacion;
import utn.frgp.pa2.bepart.util.DBData;

public class BuscarDocumentacionTask extends AsyncTask<Void, String, ArrayList<Documentacion>> {
    private static final String QUERY = "{call sp_BuscarDocumentacion(?,?,?)}";
    private Documentacion documentacion;

    public BuscarDocumentacionTask(Documentacion documentacion){
        this.documentacion = documentacion;
    }

    @Override
    public ArrayList<Documentacion> doInBackground(Void... voids) {
        Documentacion temp = null;
        ArrayList<Documentacion> resultado = new ArrayList<>();
        Connection connection = new DBData().getConnection();

        try {
            CallableStatement callableStatement = connection.prepareCall(QUERY);
            int i = 1;
            callableStatement.setInt(i++, documentacion.getIdDocumentacion());
            callableStatement.setInt(i++, documentacion.getEstadoDoc().getIdEstadoDoc());
            callableStatement.setInt(i++, documentacion.getIdUsuario());
            callableStatement.execute();
            ResultSet resultSet = callableStatement.getResultSet();
            while(resultSet.next()){
                temp = new Documentacion();
                temp.setIdDocumentacion(resultSet.getInt("IdDocumentacion"));
                temp.setIdUsuario(resultSet.getInt("IdUsuario"));
                temp.setNroDoc(resultSet.getString("NroDoc"));
                temp.setTelefono(resultSet.getString("Telefono"));
                temp.setFechaSolicitud(resultSet.getDate("FechaSolicitud"));
                temp.setImgDNIFrente(resultSet.getString("ImgDNIFrente"));
                temp.setImgDNIDorso(resultSet.getString("ImgDNIDorso"));
                temp.setImgSelfie(resultSet.getString("ImgSelfie"));
                temp.setUsername(resultSet.getString("Username"));

                EstadoDocumentacion estDoc = new EstadoDocumentacion();
                estDoc.setIdEstadoDoc(resultSet.getInt("IdEstadoDoc"));
                estDoc.setDescripcion(resultSet.getString("estDocDescripcion"));
                estDoc.setEstado(resultSet.getInt("estDocEstado"));
                temp.setEstadoDoc(estDoc);

                temp.setComentario(resultSet.getString("Comentario"));
                temp.setTipo_documento(resultSet.getString("tipo_documento"));
                resultado.add(temp);
            }
            connection.close();
        }catch(Exception ex){
            ex.printStackTrace();
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return resultado;
    }
}
