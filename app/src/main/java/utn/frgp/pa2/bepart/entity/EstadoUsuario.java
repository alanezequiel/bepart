package utn.frgp.pa2.bepart.entity;

public class EstadoUsuario {
    private int idEstado;
    private String descripcion;

    public EstadoUsuario() {

    }

    public EstadoUsuario(int idEstado, String descripcion) {
        this.idEstado = idEstado;
        this.descripcion = descripcion;
    }

    public int getIdEstado() {
        return idEstado;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setIdEstado(int idEstado) {
        this.idEstado = idEstado;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
