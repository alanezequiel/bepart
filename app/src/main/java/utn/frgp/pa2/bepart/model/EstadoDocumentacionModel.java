package utn.frgp.pa2.bepart.model;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import utn.frgp.pa2.bepart.entity.EstadoDocumentacion;
import utn.frgp.pa2.bepart.tasks.BuscarEstadoDocumentacionTask;
import utn.frgp.pa2.bepart.util.IConnectable;

public class EstadoDocumentacionModel implements IConnectable<EstadoDocumentacion> {

    @Override
    public boolean insert(EstadoDocumentacion obj) throws ExecutionException, InterruptedException {
        return false;
    }

    @Override
    public boolean update(EstadoDocumentacion obj) throws ExecutionException, InterruptedException {
        return false;
    }

    public ArrayList<EstadoDocumentacion> select(EstadoDocumentacion obj) throws ExecutionException, InterruptedException {
        if(obj != null){
            BuscarEstadoDocumentacionTask buscarTask = new BuscarEstadoDocumentacionTask(obj);
            buscarTask.execute();
            return buscarTask.get();
        }
        return null;
    }
}
