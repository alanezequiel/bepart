package utn.frgp.pa2.bepart.util;

import java.util.concurrent.ExecutionException;

public interface IConnectable<T> {
    public boolean insert(T obj) throws ExecutionException, InterruptedException;
    public boolean update(T obj) throws ExecutionException, InterruptedException;
}
