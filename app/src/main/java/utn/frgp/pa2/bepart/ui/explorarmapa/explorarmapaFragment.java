package utn.frgp.pa2.bepart.ui.explorarmapa;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.EventLog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import utn.frgp.pa2.bepart.R;
import utn.frgp.pa2.bepart.entity.Comentario;
import utn.frgp.pa2.bepart.entity.Evento;
import utn.frgp.pa2.bepart.interfaces.IComunicaFragments;
import utn.frgp.pa2.bepart.tasks.BuscarComentariosTask;
import utn.frgp.pa2.bepart.tasks.BuscarEventosTask;


public class explorarmapaFragment extends Fragment implements OnMapReadyCallback, GoogleMap.InfoWindowAdapter {

    private static final String TAG = "explorarmapaFragment";

    private View rootView;
    private GoogleMap gMap;
    private MapView mapView;
    HashMap<String, String> markerMap = new HashMap<String, String>();

    //private SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yyyy");
    //private SimpleDateFormat dh = new SimpleDateFormat("HH:mm aa");

    private Activity activity;
    private IComunicaFragments mComunicaFragments;
    private ArrayList<Evento> resultado = null;
    private Map<String, Integer> events = new HashMap<>();

    public explorarmapaFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         rootView = inflater.inflate(R.layout.fragment_explorar_mapa, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mapView = (MapView) rootView.findViewById(R.id.map);
        if (mapView != null){
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this);
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if(context instanceof Activity){
            activity = (Activity) context;
            mComunicaFragments = (IComunicaFragments) activity;
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap = googleMap;

        BuscarEventosTask buscarEventosTask = new BuscarEventosTask(0,0,getContext());
        buscarEventosTask.execute();
        try {
            resultado = buscarEventosTask.get();

        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        gMap.setMinZoomPreference(8);
        gMap.setMaxZoomPreference(20);

        for (Evento _evento : resultado) {
            LatLng place = new LatLng (_evento.getUbicacion().getLatitud(), _evento.getUbicacion().getLongitud());
            Marker marker = gMap.addMarker(new MarkerOptions()
                    .position(place)
                    .title(_evento.getTitulo())
                    .snippet(_evento.getUbicacion().getCalle() + " | " + _evento.getFecha() + " " + _evento.getHora()));

            events.put(marker.getId(), _evento.getIdEvento());

            gMap.moveCamera(CameraUpdateFactory.newLatLng(place));
        }

        gMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {

                String actionId = markerMap.get(marker.getId());

                String idMarker = "";
                int idEvento = 0;
                for(Map.Entry<String, Integer> i : events.entrySet()){
                    Log.e(TAG, "en entry set k:"+i.getKey()+" v:"+i.getValue()+" id:"+marker.getId());
                    idMarker = i.getKey();
                    idEvento = i.getValue();
                    if(idMarker.compareTo(marker.getId()) == 0){

                        break;

//                        for(Evento _evento : resultado){
//                            Log.e(TAG, "en for de eventos");
//
//                            if(_evento.getIdEvento() == i.getValue()){
//                                Log.e(TAG, "en for de eventos"+ _evento.getIdEvento()+i.getValue());
//
//                            }
//                        }
                    }
                }
                Log.e(TAG, "en asdasd set k:"+idMarker+" v:"+idEvento);

                if(idEvento != 0){
                    for(Evento _evento : resultado){
                        if(_evento.getIdEvento() == idEvento){
                            try {
                                ArrayList<Comentario> comentarios = obtenerComentarios(_evento.getIdEvento());
                                mComunicaFragments.MostrarDetalleEvento(_evento, comentarios);

                            } catch (ExecutionException e) {
                                e.printStackTrace();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }


//                    for(Evento _evento : resultado){
//                        if(marker.getTitle() == _evento.getTitulo()){
//                            LatLng place = new LatLng (_evento.getUbicacion().getLatitud(), _evento.getUbicacion().getLongitud());
//                            if(marker.getPosition() == place){
//                                eventoSeleccionado = _evento;
//                                break;
//                            }
//                        }
//                    }
            }
        });

    }

    @Override
    public View getInfoWindow(Marker marker) {

        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }


    public ArrayList<Comentario> obtenerComentarios(int idEvento) throws ExecutionException, InterruptedException {
        ArrayList<Comentario> comentarios = new ArrayList<>();

        BuscarComentariosTask comentariosTask = new BuscarComentariosTask(idEvento);
        comentariosTask.execute();
        comentarios = comentariosTask.get();

        return comentarios;
    }
}