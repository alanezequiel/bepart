/*package utn.frgp.pa2.bepart.model;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import utn.frgp.pa2.bepart.entity.EstadoDocumentacion;
import utn.frgp.pa2.bepart.entity.Pais;
import utn.frgp.pa2.bepart.tasks.AltaPaisTask;
import utn.frgp.pa2.bepart.tasks.BuscarEstadoDocumentacionTask;
import utn.frgp.pa2.bepart.tasks.BuscarPaisTask;
import utn.frgp.pa2.bepart.util.IConnectable;


public class PaisModel implements IConnectable<Pais> {

    @Override
    public boolean insert(Pais obj) throws ExecutionException, InterruptedException {
        boolean resp = false;
        if(obj != null){
            AltaPaisTask altaTask = new AltaPaisTask(obj);
            altaTask.execute();
            if(altaTask.get() != 0)
                resp = true;
            resp = false;
        }
        return resp;
    }

    @Override
    public boolean update(Pais obj) throws ExecutionException, InterruptedException {
        return false;
    }

    public ArrayList<Pais> select(Pais obj) throws ExecutionException, InterruptedException {
        if(obj != null){
            BuscarPaisTask buscarTask = new BuscarPaisTask(obj);
            buscarTask.execute();
            return buscarTask.get();
        }
        return null;
    }

}*/
