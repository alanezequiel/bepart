package utn.frgp.pa2.bepart.entity;

public class EstadoEvento {
    private int idEstadoEvento;
    private String descripcion;

    public EstadoEvento(int idEstadoEvento, String descripcion) {
        this.idEstadoEvento = idEstadoEvento;
        this.descripcion = descripcion;
    }

    public int getIdEstadoEvento() {
        return idEstadoEvento;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setIdEstadoEvento(int idEstadoEvento) {
        this.idEstadoEvento = idEstadoEvento;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
