package utn.frgp.pa2.bepart.dialogos;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import java.util.concurrent.ExecutionException;

import utn.frgp.pa2.bepart.R;
import utn.frgp.pa2.bepart.entity.CalificacionEvento;
import utn.frgp.pa2.bepart.entity.Evento;
import utn.frgp.pa2.bepart.model.CalificacionesEventosModel;
import utn.frgp.pa2.bepart.ui.eventos.MisEventosFragment;

public class DialogoCalificarEvento {
    private static final String TAG = "DialogoCalificarEvento";

    private EditText tvComentCalificar;
    private TextView tvTituloCalificar, tvTituloVentana;
    private RatingBar ratingBar;
    private float rating;

    public DialogoCalificarEvento(final Context contexto, Evento eventoSeleccionado, int idUsuario, Fragment fragment){

        CalificacionEvento calificacionEvento = new CalificacionEvento();
        calificacionEvento.setIdEvento(eventoSeleccionado.getIdEvento());
        calificacionEvento.setIdUsuario(idUsuario);
        CalificacionesEventosModel model = new CalificacionesEventosModel();
        try {
           calificacionEvento = model.get(calificacionEvento);
            Log.e(TAG, "DialogoCalificarEvento: COMENTARIO "+calificacionEvento.getComentario() + calificacionEvento.getCalificacion() );
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        final Dialog dialogo = new Dialog(contexto);

        dialogo.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogo.setCancelable(true);
        dialogo.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialogo.setContentView(R.layout.fragment_calificar_evento);

        Button btnEnviar = (Button) dialogo.findViewById(R.id.btnEnviarCalificacion);
        tvComentCalificar = dialogo.findViewById(R.id.tvComentCalificar);
        ratingBar = dialogo.findViewById(R.id.ratingBar);
        tvTituloCalificar = dialogo.findViewById(R.id.tvTituloCalificar);
        tvTituloVentana = dialogo.findViewById(R.id.tvTituloVentana);


        tvTituloCalificar.setText(eventoSeleccionado.getTitulo());

        ratingBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RatingBar bar = (RatingBar) v;
                rating = bar.getRating();
            }
        });

        Log.e(TAG, "DialogoCalificarEvento: "+eventoSeleccionado.getTitulo());
        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(contexto,"Gracias por su Calificacion!",Toast.LENGTH_LONG).show();
                MisEventosFragment eventosFragment = (MisEventosFragment) fragment;
                eventosFragment.calificarEvento(eventoSeleccionado,tvComentCalificar.getText().toString(), (int)ratingBar.getRating());
                dialogo.dismiss();
            }
        });

        if(calificacionEvento.getIdCalificacion() != 0){
            tvTituloVentana.setText(R.string.titutlo_calificar_calificado);
            ratingBar.setRating(calificacionEvento.getCalificacion());
            tvComentCalificar.setText(calificacionEvento.getComentario());
            tvComentCalificar.setFocusable(false);
            btnEnviar.setVisibility(View.INVISIBLE);
        }

        dialogo.show();
    }

}
