package utn.frgp.pa2.bepart.tasks;

import android.os.AsyncTask;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import utn.frgp.pa2.bepart.entity.Documentacion;
import utn.frgp.pa2.bepart.util.DBData;

public class AltaDocumentacionTask extends AsyncTask<Void, String, Integer> {

    private static final String QUERY ="{call sp_RegistrarDocumentacion(?,?,?,?,?,?,?,?,?)}";
    private Documentacion documentacion;

    public AltaDocumentacionTask(Documentacion documentacion){
        this.documentacion = documentacion;
    }

    @Override
    public Integer doInBackground(Void... voids) {

        Integer temp = new Integer(0);
        Connection connection = new DBData().getConnection();
        try{

            CallableStatement preparedStatement = connection.prepareCall(QUERY);

            int i = 1;
            preparedStatement.setInt(i++,this.documentacion.getIdUsuario());
            preparedStatement.setString(i++,this.documentacion.getNroDoc());
            preparedStatement.setString(i++,this.documentacion.getTelefono());
            preparedStatement.setString(i++,this.documentacion.getImgDNIFrente());
            preparedStatement.setString(i++,this.documentacion.getImgDNIDorso());
            preparedStatement.setString(i++,this.documentacion.getImgSelfie());
            preparedStatement.setInt(i++,this.documentacion.getEstadoDoc().getIdEstadoDoc());
            preparedStatement.setString(i++,this.documentacion.getComentario());
            preparedStatement.setString(i++,this.documentacion.getTipo_documento());

            temp = preparedStatement.executeUpdate();
            connection.close();
        }catch(Exception ex){
            ex.printStackTrace();
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return temp;
    }
}
