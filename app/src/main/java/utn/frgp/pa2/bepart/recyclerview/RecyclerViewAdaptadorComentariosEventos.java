package utn.frgp.pa2.bepart.recyclerview;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import utn.frgp.pa2.bepart.R;
import utn.frgp.pa2.bepart.entity.Comentario;

public class RecyclerViewAdaptadorComentariosEventos extends RecyclerView.Adapter<RecyclerViewAdaptadorComentariosEventos.ContenedorComentario>{

    private ArrayList<Comentario> comentarios;


    public RecyclerViewAdaptadorComentariosEventos(ArrayList<Comentario> _comentarios){
        this.comentarios = _comentarios;
    }

    @NonNull
    @Override
    public RecyclerViewAdaptadorComentariosEventos.ContenedorComentario onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_comentario_puntaje, parent, false);
        ContenedorComentario holder = new ContenedorComentario(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewAdaptadorComentariosEventos.ContenedorComentario holder, int position) {
        Comentario comentario = comentarios.get(position);

        holder.tvNombreUsuario.setText(comentario.getNombreUsuario());
        holder.tvComentario.setText(comentario.getComentario());
        holder.rbPuntaje.setRating((float)comentario.getPuntaje());
    }

    @Override
    public int getItemCount() {
        return comentarios.size();
    }

    public static class ContenedorComentario extends RecyclerView.ViewHolder {

        private TextView tvComentario, tvNombreUsuario;
        private RatingBar rbPuntaje;

        public ContenedorComentario(@NonNull View itemView) {
            super(itemView);
            rbPuntaje = itemView.findViewById(R.id.rbPuntaje);
            tvComentario = itemView.findViewById(R.id.tvComentario);
            tvNombreUsuario = itemView.findViewById(R.id.tvMombreUsuario);
        }

    }
}
