package utn.frgp.pa2.bepart.model;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import utn.frgp.pa2.bepart.entity.Documentacion;
import utn.frgp.pa2.bepart.tasks.ActualizarUsuarioYDocumentacionTask;
import utn.frgp.pa2.bepart.tasks.AltaDocumentacionTask;
import utn.frgp.pa2.bepart.tasks.BuscarDocumentacionTask;
import utn.frgp.pa2.bepart.util.IConnectable;


public class DocumentacionModel implements IConnectable<Documentacion> {

    @Override
    public boolean insert(Documentacion obj) throws ExecutionException, InterruptedException {
        boolean resp = false;
        if(obj != null){
            AltaDocumentacionTask altaTask = new AltaDocumentacionTask(obj);
            altaTask.execute();
            if(altaTask.get() != 0) {
                resp = true;
            }
        }
        return resp;
    }

    public ArrayList<Documentacion> select(Documentacion documentacion) throws ExecutionException, InterruptedException {
        BuscarDocumentacionTask buscarDocumentacionTask = new BuscarDocumentacionTask(documentacion);
        buscarDocumentacionTask.execute();
        return buscarDocumentacionTask.get();
    }

    @Override
    public boolean update(Documentacion obj) throws ExecutionException, InterruptedException {
        return false;
    }

    public boolean update(Documentacion obj, int idEstDoc, int idTipUsu) throws ExecutionException, InterruptedException {
        ActualizarUsuarioYDocumentacionTask actualizarUsuYDocTask = new ActualizarUsuarioYDocumentacionTask(
                obj.getIdDocumentacion(),
                obj.getIdUsuario(),
                idEstDoc,
                idTipUsu,
                obj.getComentario()
        );
        actualizarUsuYDocTask.execute();
        return (actualizarUsuYDocTask.get() != 0);
    }
}
