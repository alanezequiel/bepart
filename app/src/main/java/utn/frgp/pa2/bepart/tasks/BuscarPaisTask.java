package utn.frgp.pa2.bepart.tasks;

import android.os.AsyncTask;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import utn.frgp.pa2.bepart.entity.EstadoDocumentacion;
import utn.frgp.pa2.bepart.entity.Pais;
import utn.frgp.pa2.bepart.entity.Provincia;
import utn.frgp.pa2.bepart.entity.Zona;
import utn.frgp.pa2.bepart.util.DBData;

public class BuscarPaisTask extends AsyncTask<Void, String, ArrayList<Pais>> {
    private static final String QUERY = "{call sp_BuscarPais(?,?)}";
    private int paisInt;
    private String pais;

    public BuscarPaisTask(int paisInt, String pais) {
        this.paisInt = paisInt;
        this.pais = pais;
    }

    @Override
    public ArrayList<Pais> doInBackground(Void... voids) {
        Pais pais_temp = null;
        ArrayList<Pais> resultado = new ArrayList<>();
        Connection connection = new DBData().getConnection();

        try {
            CallableStatement callableStatement = connection.prepareCall(QUERY);

            int i = 1;

            if(this.paisInt == 0){
                callableStatement.setInt(i++, 0);
            }else{
                callableStatement.setInt(i++, this.paisInt);
            }

            if(this.pais.equals("")){
                callableStatement.setInt(i++, 0);
            }else{
                callableStatement.setString(i++, this.pais);
            }


            callableStatement.execute();
            ResultSet resultSet = callableStatement.getResultSet();
            while(resultSet.next()){
                pais_temp = new Pais();
                pais_temp.setIdPais(resultSet.getInt("idPais"));
                pais_temp.setDescripcion(resultSet.getString("Pais"));

                resultado.add(pais_temp);
            }
            connection.close();
        }catch(Exception ex){
            ex.printStackTrace();
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return resultado;
    }
}
