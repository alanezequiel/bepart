package utn.frgp.pa2.bepart.tasks;

import android.os.AsyncTask;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import utn.frgp.pa2.bepart.entity.AsistenciaxEvento;
import utn.frgp.pa2.bepart.entity.CalificacionEvento;
import utn.frgp.pa2.bepart.entity.Categoria;
import utn.frgp.pa2.bepart.entity.EstadoEvento;
import utn.frgp.pa2.bepart.entity.Evento;
import utn.frgp.pa2.bepart.entity.Pais;
import utn.frgp.pa2.bepart.entity.Provincia;
import utn.frgp.pa2.bepart.entity.Ubicacion;
import utn.frgp.pa2.bepart.entity.Usuario;
import utn.frgp.pa2.bepart.entity.Zona;
import utn.frgp.pa2.bepart.util.DBData;

public class BuscarCalificacionEventoTask extends AsyncTask<Void, String, CalificacionEvento> {

    private static final String QUERY = "{call sp_BuscarCalificacionEvento(?,?)}";
    private int idUsuario;
    private int idEvento;

    public BuscarCalificacionEventoTask(int idUsuario, int idEvento) {
        this.idUsuario = idUsuario;
        this.idEvento = idEvento;
    }

    @Override
    protected CalificacionEvento doInBackground(Void... voids) {
        CalificacionEvento evento = null;
        Connection connection = new DBData().getConnection();

        try {
            CallableStatement callableStatement = connection.prepareCall(QUERY);
            int i = 1;

            if(this.idUsuario == 0){
                callableStatement.setInt(i++, 0);
            }else{
                callableStatement.setInt(i++, this.idEvento);
            }

            if(this.idEvento == 0){
                callableStatement.setInt(i++, 0);
            }else{
                callableStatement.setInt(i++, this.idUsuario);
            }

            callableStatement.execute();
            ResultSet resultSet = callableStatement.getResultSet();
            resultSet.next();

                evento = new CalificacionEvento();
                evento.setIdCalificacion(resultSet.getInt("IdCalificacion"));
                evento.setComentario(resultSet.getString("Comentario"));
                evento.setIdEvento(resultSet.getInt("IdEvento"));
                evento.setIdUsuario(resultSet.getInt("IdUsuario"));
                evento.setFechaCalificacion(resultSet.getDate("FechaCalificacion"));
                evento.setCalificacion(resultSet.getInt("Calificacion"));

            connection.close();
        }catch(Exception ex){
            ex.printStackTrace();
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return  evento;
    }
}
