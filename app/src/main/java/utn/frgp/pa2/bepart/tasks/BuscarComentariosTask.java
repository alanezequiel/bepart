package utn.frgp.pa2.bepart.tasks;

import android.os.AsyncTask;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import utn.frgp.pa2.bepart.entity.Comentario;
import utn.frgp.pa2.bepart.util.DBData;

public class BuscarComentariosTask  extends AsyncTask<Void, String, ArrayList<Comentario>> {
    private static final String QUERY = "{call sp_BuscarComentarios(?)}";
    private int idEvento;

    public BuscarComentariosTask(int idEvento){
        this.idEvento = idEvento;
    }

    @Override
    public ArrayList<Comentario> doInBackground(Void... voids) {
        Comentario temp = null;
        ArrayList<Comentario> comentarios = new ArrayList<>();

        Connection connection = new DBData().getConnection();

        try {

            CallableStatement callableStatement = connection.prepareCall(QUERY);

            int i = 1;

            callableStatement.setInt(i++, idEvento);

            callableStatement.execute();
            ResultSet resultSet = callableStatement.getResultSet();
            while(resultSet.next()){
                temp = new Comentario();
                temp.setComentario(resultSet.getString("Comentario"));
                temp.setNombreUsuario(resultSet.getString("UserName"));
                temp.setPuntaje(resultSet.getInt("Puntaje"));

                comentarios.add(temp);
            }
            connection.close();
        }catch(Exception ex){
            ex.printStackTrace();
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return comentarios;
    }
}
