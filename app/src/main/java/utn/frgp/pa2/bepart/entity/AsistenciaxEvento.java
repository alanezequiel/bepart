package utn.frgp.pa2.bepart.entity;

public class AsistenciaxEvento {
    private int idAsistencia;
    private Usuario usuario;
    private Evento evento;


    public AsistenciaxEvento(int idAsistencia, Usuario usuario, Evento evento){
        this.idAsistencia = idAsistencia;
        this.usuario = usuario;
        this.evento = evento;
    };

    public AsistenciaxEvento(){

    }

    public int getIdAsistencia() {
        return idAsistencia;
    }

    public void setIdAsistencia(int idAsistencia) {
        this.idAsistencia = idAsistencia;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Evento getEvento() {
        return evento;
    }

    public void setEvento(Evento evento) {
        this.evento = evento;
    }
}
