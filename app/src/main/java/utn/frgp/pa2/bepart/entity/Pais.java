package utn.frgp.pa2.bepart.entity;

public class Pais {
    private int idPais;
    private String descripcion;

    public Pais(){};

    public Pais(int idPais, String descripcion) {
        this.idPais = idPais;
        this.descripcion = descripcion;
    }

    public int getIdPais() {
        return idPais;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setIdPais(int idPais) {
        this.idPais = idPais;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
