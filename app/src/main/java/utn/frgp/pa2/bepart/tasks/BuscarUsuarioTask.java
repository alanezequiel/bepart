package utn.frgp.pa2.bepart.tasks;

import android.os.AsyncTask;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;

import utn.frgp.pa2.bepart.entity.EstadoUsuario;
import utn.frgp.pa2.bepart.entity.TipoUsuario;
import utn.frgp.pa2.bepart.util.*;
import utn.frgp.pa2.bepart.entity.Usuario;

public class BuscarUsuarioTask extends AsyncTask<Void, String, ArrayList<Usuario>> {
    private static final String QUERY = "{call sp_BuscarUsuario(?,?)}";
    private Usuario interno;

    public BuscarUsuarioTask(Usuario externo){
        this.interno = externo;
    }
    @Override
    public ArrayList<Usuario> doInBackground(Void... voids) {
        Usuario usuario_temp = null;
        ArrayList<Usuario> resultado = new ArrayList<>();
        Connection connection = new DBData().getConnection();

        try {

            CallableStatement callableStatement = connection.prepareCall(QUERY);

            int i = 1;

            if(this.interno.getUsername() == null){
                callableStatement.setNull(i++, Types.NULL);
            }else{
                callableStatement.setString(i++, this.interno.getUsername());
            }

            if(this.interno.getEmail() == null){
                callableStatement.setNull(i++, Types.NULL);
            }else{
                callableStatement.setString(i++, this.interno.getEmail());
            }

            callableStatement.execute();
            ResultSet resultSet = callableStatement.getResultSet();
            while(resultSet.next()){
                usuario_temp = new Usuario();
                usuario_temp.setIdUsuario(resultSet.getInt("idUsuario"));
                usuario_temp.setUsername(resultSet.getString("Username"));
                usuario_temp.setNombre(resultSet.getString("Nombre"));
                usuario_temp.setApellido(resultSet.getString("Apellido"));
                usuario_temp.setRazonSocial(resultSet.getString("RazonSocial"));
                usuario_temp.setEmail(resultSet.getString("Email"));
                usuario_temp.setTipoUsuario(new TipoUsuario(resultSet.getInt("idTipoUsuario"), resultSet.getString("TipoUsuario")));
                usuario_temp.setEstadoUsuario(new EstadoUsuario(resultSet.getInt("idEstado"), resultSet.getString("EstadoUsuario")));
                usuario_temp.setPassword(resultSet.getString("Password"));
                usuario_temp.setActivo(resultSet.getInt("Activo"));
                resultado.add(usuario_temp);
            }
            connection.close();
        }catch(Exception ex){
            ex.printStackTrace();
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return resultado;
    }
}
