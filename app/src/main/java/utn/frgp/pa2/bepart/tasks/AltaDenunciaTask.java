package utn.frgp.pa2.bepart.tasks;

import android.os.AsyncTask;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import utn.frgp.pa2.bepart.util.DBData;

public class AltaDenunciaTask extends AsyncTask<Void, String, Integer> {

    private static final String QUERY ="{call sp_RegistrarDenuncia(?,?,?,?)}";
    private int idEvento;
    private int idUsuario;
    private int idTipoDenuncia;
    private String comentarios;

    public AltaDenunciaTask(int idEvento, int idUsuario, int idTipoDenuncia, String comentarios){
        this.idEvento = idEvento;
        this.idUsuario = idUsuario;
        this.idTipoDenuncia = idTipoDenuncia;
        this.comentarios = comentarios;
    }
    @Override
    public Integer doInBackground(Void... voids) {

        Integer temp = new Integer(0);
        Connection connection = new DBData().getConnection();
        try{

            CallableStatement preparedStatement = connection.prepareCall(QUERY);

            int i = 1;
            preparedStatement.setInt(i++, this.idEvento);
            preparedStatement.setInt(i++, this.idUsuario);
            preparedStatement.setInt(i++, this.idTipoDenuncia);
            preparedStatement.setString(i++, this.comentarios);

            temp = preparedStatement.executeUpdate();
            connection.close();
        }catch(Exception ex){
            ex.printStackTrace();
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return temp;
    }
}
