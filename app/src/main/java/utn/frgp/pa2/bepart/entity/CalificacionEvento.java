package utn.frgp.pa2.bepart.entity;

import java.sql.Date;

public class CalificacionEvento {

    private int idCalificacion;
    private int idUsuario;
    private int idEvento;
    private String comentario;
    private int calificacion;
    private Date fechaCalificacion;

    public CalificacionEvento(int idCalificacion, int idUsuario, int idEvento, String comentario, int calificacion, Date fechaCalificacion) {
        this.idCalificacion = idCalificacion;
        this.idUsuario = idUsuario;
        this.idEvento = idEvento;
        this.comentario = comentario;
        this.calificacion = calificacion;
        this.fechaCalificacion = fechaCalificacion;
    }

    public CalificacionEvento() {
    }

    public int getIdCalificacion() {
        return idCalificacion;
    }

    public void setIdCalificacion(int idCalificacion) {
        this.idCalificacion = idCalificacion;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public int getIdEvento() {
        return idEvento;
    }

    public void setIdEvento(int idEvento) {
        this.idEvento = idEvento;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public int getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(int calificacion) {
        this.calificacion = calificacion;
    }

    public Date getFechaCalificacion() {
        return fechaCalificacion;
    }

    public void setFechaCalificacion(Date fechaCalificacion) {
        this.fechaCalificacion = fechaCalificacion;
    }
}
