package utn.frgp.pa2.bepart.tasks;

import android.os.AsyncTask;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import utn.frgp.pa2.bepart.entity.Usuario;
import utn.frgp.pa2.bepart.util.DBData;

public class AltaUsuarioTask extends AsyncTask<Void, String, Integer> {

    private static final String QUERY ="{call sp_RegistrarUsuario(?,?,?,?,?)}";
    private Usuario interno;
    public AltaUsuarioTask(Usuario externo){
        this.interno = externo;
    }
    @Override
    public Integer doInBackground(Void... voids) {

        Integer temp = new Integer(0);
        Connection connection = new DBData().getConnection();
        try{

            CallableStatement preparedStatement = connection.prepareCall(QUERY);

            int i = 1;
            preparedStatement.setString(i++,this.interno.getUsername ());
            preparedStatement.setString(i++,this.interno.getNombre());
            preparedStatement.setString(i++,this.interno.getApellido());
            preparedStatement.setString(i++, this.interno.getEmail());
            preparedStatement.setString(i++, this.interno.getPassword());
            temp = preparedStatement.executeUpdate();
            connection.close();
        }catch(Exception ex){
            ex.printStackTrace();
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return temp;
    }
}
